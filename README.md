#Hey there!

Yes, you :)

This is the code repository for the OVERRIDE – Render Layer Overrides add-on, where you can always find the latest version available for download.  
Since it is licensed as [GPL](http://www.gnu.org/copyleft/gpl.html "GPL License"), which means it's free and you can modify and redistribute it to as you want (as long as you keep the original license),  
you can download it for free.

#But!

$19.99 isn't much and every single sale keeps me afloat and motivates me to keep developing it.  
It would make my day if you bought a copy :) Plus a small Percent of income goes to the Blender Dev Fund.

So if you'd like to support me, just head on over to the Blender Market and buy it there!

- - -
![Feature_Image](http://www.lucascheller.de/A_Image_Host/OVERRIDE_Add-on/Feature_Image.png)

OVERRIDE – Render Layer Overrides 
===============

OVERRIDE is a Blender add-on which allows you to override settings for buttons per render layer in an artist friendly and intuitive way.  
This gives you the advantage of optimizing render settings, materials and more per render layer.  

Intuitive OVERRIDE Management
---------------

Adding overrides is as easy as inserting a keyframe. Just press O / Alt +O over a button where you want to add/remove an override. When switching   
render layers in the “OVERRIDE Render Layers” panel, the add-on will remember your settings and load them for the active layer. What essentially   
is done behind the scenes is that the add-on adds a driver to the button and then saves and loads the corresponding settings based on what render  
layer you are on. Render Layers that don’t have an override automatically get set to the default value. To change the default value, go to the   
“OVR_Default_RenderLayer” Render Layer and set your desired value.  If you want to edit override values you have three options: 

-	You can go to the “OVERRIDE Driver List” panel, where you can find a list of all OVERRIDE drivers. This is the easiest since the panel offers     
	a nice overview about the active OVERRIDE driver as well as an operator panel, which allows you to adjust the active OVERRIDE driver easily.
-	Secondly you can go to the button manually and edit the values via the hotkeys there
-	Thirdly you can go to the drivers panel in the graph editor and add your keyframes manually for maximum control, but there are a couple of   
	things you have to consider.  For more details visit the Documentation and watch the tutorial video.

The Rendering Process
---------------

The rendering process is slightly different since Blender doesn’t offer the possibility to call scripts between rendering different render layers:

1.	Enable “Override Render Hotkeys” (Optional)
2.	Render your Images via the “OVERRIDE Render Menu” panel Render Buttons or via the above activated Hotkeys

For Cycles, Blender Internal and more
---------------

Since this add-on is based on blender’s render layer system you can use it with any render engine that relies on this system. So whether your production  
uses the old but customizable Blender Internal Render or the new more physically based Cycles this add-on has got you covered.  

Support and Future Features
---------------

Feel free to leave suggestions for what could be implemented into the add-on in the near future. My current to-do list includes adding support for multiple   
scenes in one .blend file, copying and pasting overrides and handling duplicating objects with overrides. For a detailed list of to-do features, see the Documentation.        

- - -

![Product_Images](http://www.lucascheller.de/A_Image_Host/OVERRIDE_Add-on/Product_Images.jpg)




