# BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# END GPL LICENSE BLOCK #####


bl_info = {
    "name": "Render Layer OVERRIDE",
    "author": "Luca Scheller",
    "version": (1,1),
    "blender": (2, 73, 0),
    "location": "Render Layer Panel | O : Add Override & Alt + O : Remover Override",
    "description": "Override Button Settings per Render Layer",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Render"
}


import bpy, os, re, time
from bpy.props import *
from bpy.types import Panel
from bpy.app.handlers import persistent

############################################################################################################### Add-on Execution/File Save/File Load Functions

def OVR_Addon_First_ExecutionFunction(scene): # Add-on UserPref Activation
  
    # Safety Restrictions Override via Scene Update Handler --> Hack / Not a clean solution
    bpy.app.handlers.scene_update_post.remove(OVR_Addon_First_ExecutionFunction)
  
    OVR_CreateDriverListFunction()
    OVR_CreateDriverSettingsListFunction()
    OVR_CreateDefaultRenderLayerFunction()
    
    # Hotkey Override Startup Execution (since the prop collection doesn't get deleted)
    if bpy.data.scenes[bpy.context.scene.name].OVR_DriverSettings.Override_Render_Hotkeys == True:
        OVR_Override_Render_Hotkeys_Add_Function()
    
    print("OVR | Add-on Activated")
    
@persistent
def OVR_Addon_ExecutionFunction(scene): # Add-on Scene Load -> All Globals/Frame Handlers are cleared on scene load automatically
  
    OVR_CreateDriverListFunction()
    OVR_CreateDriverSettingsListFunction()
    OVR_CreateDefaultRenderLayerFunction()
    
    # Hotkey Override Startup Execution (since the prop collection doesn't get deleted)
    if bpy.data.scenes[bpy.context.scene.name].OVR_DriverSettings.Override_Render_Hotkeys == True:
        OVR_Override_Render_Hotkeys_Add_Function()
    
    print("OVR | Add-on Activated")

def OVR_CreateDriverListFunction():
  
    # Globals
    global DriverList_GlobalList
    global NAP_AcceptDict
    global NAP_Warning_Panel_Functions   
    global NAP_Frame_Handler_Functions   
    # Variables
    scene_name = bpy.context.scene.name  
    NAP_AcceptDict_PropertyNameList = []
    for k in NAP_AcceptDict:
        NAP_AcceptDict_PropertyNameList.append(NAP_AcceptDict[k][NAP_AcceptDict[k].rfind(",")+1:]) 
  
    if str(DriverList_GlobalList) == "set()":
        for x in bpy.data.scenes.values():
            OVR_NAP_Reload_CreateDriverListSubFunction(NAP_AcceptDict,NAP_AcceptDict_PropertyNameList,x,"scenes") 
            OVR_CreateDriverListSubFunction(x)
     
        for x in bpy.data.objects.values():
            OVR_NAP_Reload_CreateDriverListSubFunction(NAP_AcceptDict,NAP_AcceptDict_PropertyNameList,x,"objects") 
            OVR_CreateDriverListSubFunction(x)    
            if x.data != None:    
                OVR_CreateDriverListSubFunction(x.data)
                if x.type == 'LAMP':
                    if x.data.use_nodes == True:
                        OVR_CreateDriverListSubFunction(x.data.node_tree) 
            
        for x in bpy.data.worlds.values():
            OVR_NAP_Reload_CreateDriverListSubFunction(NAP_AcceptDict,NAP_AcceptDict_PropertyNameList,x,"worlds") 
            OVR_CreateDriverListSubFunction(x)       
            if x.use_nodes == True:      
                OVR_CreateDriverListSubFunction(x.node_tree)      
            
        for x in bpy.data.materials.values():
            OVR_NAP_Reload_CreateDriverListSubFunction(NAP_AcceptDict,NAP_AcceptDict_PropertyNameList,x,"materials") 
            OVR_CreateDriverListSubFunction(x)  
            if x.use_nodes == True:      
                OVR_CreateDriverListSubFunction(x.node_tree)   
        
        for x in bpy.data.particles.values():
            OVR_NAP_Reload_CreateDriverListSubFunction(NAP_AcceptDict,NAP_AcceptDict_PropertyNameList,x,"particles") 
            OVR_CreateDriverListSubFunction(x)    
    
        # Reset Driver Counter if no Drivers exist
        if len(DriverList_GlobalList) == 0:
            bpy.data.scenes[0].OVR_DriverSettings.DriverAmount_GlobalIndex = 0

    # Generate NAP Panels & Frame Handlers
    if NAP_Warning_Panel_Functions != []: # Unnecessary Check
        for x in NAP_Warning_Panel_Functions:
            rna_path = NAP_dict_value_search(NAP_AcceptDict, x.__name__)
            temp_panel_name = NAP_AcceptDict[rna_path][:NAP_AcceptDict[rna_path].rfind(",")]
            exec("bpy.types."+temp_panel_name+".prepend(x)")
    if NAP_Frame_Handler_Functions != []: # Unnecessary Check
        for x in NAP_Frame_Handler_Functions:
            bpy.app.handlers.frame_change_post.append(x)

def OVR_CreateDriverListSubFunction(DriverList_GlobalList_SearchPaths):
  
    #Globals
    global DriverList_GlobalList
    
    DriverList_GlobalList_SearchPaths.animation_data_create()
    if DriverList_GlobalList_SearchPaths.animation_data.drivers.values() != []:
        for y in DriverList_GlobalList_SearchPaths.animation_data.drivers.values():
            if len(y.driver.variables) > 0:
                if str(y.driver.variables[0].name)[:10] == "OVR_Driver":
                    DriverList_GlobalList.add(y)

def OVR_NAP_Reload_CreateDriverListSubFunction(NAP_AD,NAP_AD_PNL,id_block,id_block_name):
  
    for x in NAP_AD_PNL:
        if x in id_block.keys():
            if id_block[x] != -12345610: 
                # Variables
                if id_block_name == 'particles':
                    id_type = 'ParticleSettings'
                else:
                    id_type = id_block_name.title()[:-1]
                exec("bpy.types."+id_type+"."+x+" = bpy.props.FloatProperty(name='"+x+"')") # "Re-Activate" Property
                for k in NAP_AD:
                    if NAP_AD[k][NAP_AD[k].rfind(",")+1:] == x:
                        rna_path = k 
                        break
                c_prop_name = x
                prop_name = rna_path
                # Get Driver ID
                id_block.animation_data_create()
                if id_block.animation_data.drivers.values() != []: # Unneccesary
                    for y in id_block.animation_data.drivers.values():
                        if y.data_path == x:
                            driver_id = y.driver.variables[0].name
                            break
                
                # Re-Generate Warning Button Panel Functions
                NAP_Warning_Panel_FunctionBuilder(c_prop_name,id_block_name)
                # Re-Generate Frame Handler Functions  
                NAP_Frame_Change_Post_Handler_FunctionBuilder(id_block, c_prop_name, prop_name, driver_id) 
                        
def OVR_CreateDriverSettingsListFunction():
  
    # Globals
    global Driver_Settings_Default
    global Driver_Settings_RL 
    
    # Variables
    scene_name = bpy.context.scene.name    
    if bpy.data.scenes[scene_name].OVR_DriverSettings.Settings_RL ==  "":
        for x in bpy.data.scenes[scene_name].render.layers:
            if x.name != "OVR_Default_RenderLayer":
                Driver_Settings_RL.append({})
        bpy.data.scenes[scene_name].OVR_DriverSettings.Settings_RL =  "Placeholder"
    else:
        if bpy.data.scenes[scene_name].OVR_DriverSettings.Settings_RL !=  "Placeholder":
            Driver_Settings_Default = eval(bpy.data.scenes[scene_name].OVR_DriverSettings.Settings_Default)
            Driver_Settings_RL = eval(bpy.data.scenes[scene_name].OVR_DriverSettings.Settings_RL)

def OVR_CreateDefaultRenderLayerFunction():
  
    # Variables
    scene_name = bpy.context.scene.name
    DefaultRenderLayerExists = False
    
    for x in bpy.data.scenes[scene_name].render.layers.values():
        if x.name == "OVR_Default_RenderLayer":
            DefaultRenderLayerExists = True
    if DefaultRenderLayerExists == False:
        OVR_Default_RenderLayer = bpy.data.scenes[scene_name].render.layers.new("OVR_Default_RenderLayer") 
        OVR_Default_RenderLayer_Settings_Function(OVR_Default_RenderLayer)

@persistent
def OVR_FileSaveFunction(scene):
  
    # Globals
    global Driver_Settings_Default
    global Driver_Settings_RL
    
    # Variavbles
    scene_name = bpy.context.scene.name 
    
    # Reset Scene Variables
    bpy.data.scenes[scene_name]["OVR_DriverSettings"]["Settings_Default"] = ""
    bpy.data.scenes[scene_name]["OVR_DriverSettings"]["Settings_RL"] = ""
    
    # Trigger Render Layer Update To Save Latest Changes
    bpy.data.scenes[scene_name].OVR_ActiveRenderLayerIndex = bpy.data.scenes[scene_name].render.layers.active_index 
    
    # Save Settings Lists
    bpy.data.scenes[scene_name].OVR_DriverSettings.Settings_Default = str(Driver_Settings_Default)
    bpy.data.scenes[scene_name].OVR_DriverSettings.Settings_RL = str(Driver_Settings_RL)
    
    print("OVR | Driver Settings Saved")
        
    
############################################################################################################### Add-on Execution Functions

############################################################################################################### Driver Functions

def OVR_GetButtonDataFunction(self):
    
    if bpy.ops.ui.copy_data_path_button.poll() == False:
        return False
    
    # Globals
    global Active_List_Driver
        
    # Get Button Path Data 
    clipboard_safe = bpy.context.window_manager.clipboard
    bpy.ops.ui.copy_data_path_button() 
    rna_path = bpy.context.window_manager.clipboard
    bpy.context.window_manager.clipboard = clipboard_safe
    scene_name = bpy.context.scene.name     
    if bpy.context.scene.world != None:  
        world_name = bpy.context.scene.world.name  
    if bpy.context.scene.objects.active != None:
        object_name =  str(bpy.context.scene.objects.active.name)
        self.dependency_graph_hack_base_path = bpy.data.objects[object_name]
        data_path_active_obj = bpy.data.objects[object_name]
        if data_path_active_obj.type == 'MESH':
            data_path_active_obj_sub = bpy.data.meshes[bpy.data.objects[object_name].data.name]
            self.driver_target_id_type = 'MESH'
            self.driver_target_data_path = "MESH_DATA,None"
        elif data_path_active_obj.type == 'LAMP':
            data_path_active_obj_sub = bpy.data.lamps[bpy.data.objects[object_name].data.name]
            self.driver_target_id_type = 'LAMP'
            self.driver_target_data_path = "LAMP,None"
        elif data_path_active_obj.type == 'CAMERA':
            data_path_active_obj_sub = bpy.data.cameras[bpy.data.objects[object_name].data.name]
            self.driver_target_id_type = 'CAMERA'
            self.driver_target_data_path = "CAMERA_DATA,None"
        elif data_path_active_obj.type == 'ARMATURE':
            data_path_active_obj_sub = bpy.data.armatures[bpy.data.objects[object_name].data.name]
            self.driver_target_id_type = 'ARMATURE'
            self.driver_target_data_path = "ARMATURE_DATA,None"
        elif data_path_active_obj.type == 'LATTICE':
            data_path_active_obj_sub = bpy.data.lattices[bpy.data.objects[object_name].data.name]
            self.driver_target_id_type = 'LATTICE'
            self.driver_target_data_path = "LATTICE_DATA,None"
    if bpy.context.area.type == "PROPERTIES":
        property_tab = bpy.context.space_data.context
        
    # Evaluate Button Path Data for Property Panel 
    if bpy.context.area.type == "PROPERTIES":
        if property_tab == 'RENDER' or property_tab == 'SCENE':
            self.driver_obase_path = bpy.data.scenes[scene_name]
            self.driver_orna_path = rna_path
            self.driver_base_path = bpy.data.scenes[scene_name]
            self.driver_rna_path = rna_path    
            self.driver_target_id_type = 'SCENE'
            self.driver_target_id = bpy.data.scenes[scene_name]
            self.driver_target_data_path = "SCENE,None"
        if property_tab == 'RENDER_LAYER': # Cancel Operator if over wrong button
            if rna_path == Active_List_Driver.data_path:
                self.driver_obase_path = Active_List_Driver.driver.variables[0].targets[0].id
                self.driver_orna_path = Active_List_Driver.data_path
                self.driver_base_path = Active_List_Driver.driver.variables[0].targets[0].id
                self.driver_rna_path = Active_List_Driver.data_path    
                if rna_path[:5] == "nodes":
                    temporary_obase_path = self.driver_obase_path
                    self.driver_obase_path = temporary_obase_path.node_tree
                    self.driver_base_path = temporary_obase_path.node_tree
                self.driver_target_id_type = Active_List_Driver.driver.variables[0].targets[0].id_type
                self.driver_target_id = Active_List_Driver.driver.variables[0].targets[0].id
                self.driver_target_data_path = Active_List_Driver.driver.variables[0].targets[0].data_path 
            else:
                return False
        elif property_tab == 'WORLD':
            self.driver_target_id_type = 'WORLD'
            self.driver_target_id = bpy.data.worlds[world_name]
            self.driver_target_data_path = "WORLD,None"
            if rna_path[:5] == "nodes":
                self.driver_obase_path = bpy.data.worlds[world_name].node_tree
                self.driver_base_path = bpy.data.worlds[world_name].node_tree
                temporary_path = self.driver_obase_path.path_resolve((rna_path.replace("default_value","name")))
                self.driver_target_data_path = "WORLD,"+temporary_path
            else:
                self.driver_obase_path = bpy.data.worlds[world_name]
                self.driver_base_path = bpy.data.worlds[world_name]
            self.driver_orna_path = rna_path
            self.driver_rna_path = rna_path 
        elif property_tab == 'OBJECT':
            self.driver_obase_path = data_path_active_obj
            self.driver_orna_path = rna_path
            self.driver_base_path = data_path_active_obj
            self.driver_rna_path = rna_path
            self.driver_target_id_type = 'OBJECT'
            self.driver_target_id = data_path_active_obj
            self.driver_target_data_path = "OBJECT_DATA,None"
        elif property_tab == 'CONSTRAINT':
            p_constraint, p_partition, p_option = rna_path.partition('.')
            s_constraint= p_constraint[13:-2]
            self.driver_obase_path = data_path_active_obj
            self.driver_orna_path = rna_path
            self.driver_base_path = data_path_active_obj.constraints[s_constraint]
            self.driver_rna_path = p_option
            self.driver_target_id_type = 'OBJECT'
            self.driver_target_id = data_path_active_obj
            self.driver_target_data_path = "CONSTRAINT,None"
        elif property_tab == 'MODIFIER': 
            p_modifier, p_partition, p_option = rna_path.partition('.')
            s_modifier = p_modifier[11:-2]
            self.driver_obase_path = data_path_active_obj
            self.driver_orna_path = rna_path
            self.driver_base_path = data_path_active_obj.modifiers[s_modifier]
            self.driver_rna_path = p_option
            self.driver_target_id_type = 'OBJECT'
            self.driver_target_id = data_path_active_obj
            self.driver_target_data_path = "MODIFIER,None"
        elif property_tab == 'DATA':
            self.driver_obase_path = data_path_active_obj_sub
            self.driver_orna_path = rna_path
            self.driver_base_path = data_path_active_obj_sub
            self.driver_rna_path = rna_path
            self.driver_target_id = data_path_active_obj_sub
            if data_path_active_obj.type == 'LAMP':
                if rna_path[:5] == "nodes":
                    self.driver_obase_path = data_path_active_obj_sub.node_tree
                    self.driver_base_path = data_path_active_obj_sub.node_tree   
                    temporary_path = self.driver_obase_path.path_resolve((rna_path.replace("default_value","name")))
                    self.driver_target_data_path = "LAMP,"+temporary_path
        elif property_tab == 'MATERIAL':
            active_material_index = data_path_active_obj.active_material_index
            active_material_name = data_path_active_obj.data.materials[active_material_index].name
            self.driver_target_id_type = 'MATERIAL'
            self.driver_target_id = bpy.data.materials[active_material_name]
            self.driver_target_data_path = "MATERIAL,None"
            if rna_path[:5] == "nodes":
                self.driver_obase_path = bpy.data.materials[active_material_name].node_tree
                self.driver_base_path = bpy.data.materials[active_material_name].node_tree
                temporary_path = self.driver_obase_path.path_resolve((rna_path.replace("default_value","name")))
                self.driver_target_data_path = "MATERIAL,"+temporary_path
            else:
                self.driver_obase_path = bpy.data.materials[active_material_name]
                self.driver_base_path = bpy.data.materials[active_material_name]
            self.driver_orna_path = rna_path       
            self.driver_rna_path = rna_path
        elif property_tab == 'PARTICLES':
            active_ps_index = data_path_active_obj.particle_systems.active_index
            active_pss_name = data_path_active_obj.particle_systems[active_ps_index].settings.name
            self.driver_obase_path = bpy.data.particles[active_pss_name]
            self.driver_orna_path = rna_path
            self.driver_base_path = bpy.data.particles[active_pss_name]
            self.driver_rna_path = rna_path
            self.driver_target_id_type = 'PARTICLE'
            self.driver_target_id = bpy.data.particles[active_pss_name]
            self.driver_target_data_path = "PARTICLES,None"
            if rna_path[:6] == "cycles": 
                self.driver_obase_path = bpy.data.scenes[scene_name]
                self.driver_base_path = bpy.data.scenes[scene_name]   
                self.driver_target_id_type = 'SCENE'
                self.driver_target_id = bpy.data.scenes[scene_name]
                self.driver_target_data_path = "SCENE,None"

    # Evaluate Button Path Data for Noder Editor
    if bpy.context.area.type == "NODE_EDITOR": 
        if bpy.context.space_data.tree_type == "ShaderNodeTree":
            if bpy.context.space_data.shader_type == "OBJECT":
                if data_path_active_obj.type == 'LAMP':
                    self.driver_obase_path = data_path_active_obj_sub.node_tree
                    self.driver_orna_path = rna_path
                    self.driver_base_path = data_path_active_obj_sub.node_tree
                    self.driver_rna_path = rna_path   
                    self.driver_target_id_type = 'LAMP'
                    self.driver_target_id = data_path_active_obj_sub
                    temporary_path = self.driver_obase_path.path_resolve((rna_path.replace("default_value","name")))
                    self.driver_target_data_path = "LAMP,"+temporary_path
                else:
                    active_material_index = data_path_active_obj.active_material_index
                    active_material_name = data_path_active_obj.data.materials[active_material_index].name
                    self.driver_obase_path = bpy.data.materials[active_material_name].node_tree
                    self.driver_orna_path = rna_path
                    self.driver_base_path = bpy.data.materials[active_material_name].node_tree
                    self.driver_rna_path = rna_path
                    self.driver_target_id_type = 'MATERIAL'
                    self.driver_target_id = bpy.data.materials[active_material_name]
                    temporary_path = self.driver_obase_path.path_resolve((rna_path.replace("default_value","name")))
                    self.driver_target_data_path = "MATERIAL,"+temporary_path
            if bpy.context.space_data.shader_type == "WORLD":
                self.driver_obase_path = bpy.data.worlds[world_name].node_tree
                self.driver_orna_path = rna_path
                self.driver_base_path = bpy.data.worlds[world_name].node_tree
                self.driver_rna_path = rna_path
                self.driver_target_id_type = 'WORLD'
                self.driver_target_id = bpy.data.worlds[world_name]
                temporary_path = self.driver_obase_path.path_resolve((rna_path.replace("default_value","name")))
                self.driver_target_data_path = "WORLD,"+temporary_path

    # Evaluate Button Path Data for Outliner
    if bpy.context.area.type == "OUTLINER": 
        self.driver_obase_path = data_path_active_obj
        self.driver_orna_path = rna_path
        self.driver_base_path = data_path_active_obj
        self.driver_rna_path = rna_path
        self.driver_target_id_type = 'OBJECT'
        self.driver_target_id = data_path_active_obj
        self.driver_target_data_path = "OBJECT_DATA,None"
    
    # Cancel Operator if Property does not support Anim_Data    
    try:
        Fake_Poll_Return = self.driver_obase_path.keyframe_delete(self.driver_orna_path,frame=-1000000)
        if Fake_Poll_Return == False:
            Fake_Poll = True
    except RuntimeError:
        Fake_Poll = True
    except AttributeError:
        Fake_Poll = False
    except TypeError:
        # NAP Conversion Process:
        global NAP_AcceptDict
        if rna_path in NAP_AcceptDict:
            if self.driver_target_id_type == 'PARTICLE':
                id_type = 'ParticleSettings'
            else:
                id_type = self.driver_target_id_type.lower().title()
            temp_type = "bpy.types."+id_type
            temp_prop_name = NAP_AcceptDict[rna_path][NAP_AcceptDict[rna_path].rfind(",")+1:]
            # Check if "Fake" Prop already exists
            try: 
                self.driver_obase_path.path_resolve(NAP_AcceptDict[rna_path][NAP_AcceptDict[rna_path].rfind(",")+1:]) # try this
                # Update Default Value 
                exec("self.driver_obase_path."+temp_prop_name+" = self.driver_obase_path."+rna_path)
                # Generate c_prop_name
                c_prop_name = NAP_AcceptDict[rna_path][NAP_AcceptDict[rna_path].rfind(",")+1:]
            except ValueError:
                # Create Property
                exec(temp_type+"."+temp_prop_name+" = bpy.props.FloatProperty(name='"+temp_prop_name+"',default=-12345610)")
                # Generate Default Value (to make it unique form other id blocks)
                exec("self.driver_obase_path."+temp_prop_name+" = self.driver_obase_path."+rna_path)
                # Generate c_prop_name 
                c_prop_name = temp_prop_name
            self.NAP_driver_original_rna_path = rna_path
            self.driver_orna_path = c_prop_name
            self.driver_rna_path = c_prop_name
            Fake_Poll = True
        else:
          Fake_Poll = False       
 
    if Fake_Poll == False:
        return False
    
    # Check if Driver is a Vector/"List"
    self.driver_button_type = type(self.driver_base_path.path_resolve(self.driver_rna_path))   
    if str(self.driver_button_type) == "<class 'Vector'>" or str(self.driver_button_type) == "<class 'Euler'>" or str(self.driver_button_type) == "<class 'Quaternion'>" or str(self.driver_button_type) == "<class 'Color'>" or str(self.driver_button_type) == "<class 'bpy_prop_array'>":
        if property_tab != 'RENDER_LAYER':
            self.driver_isVector = True   

def OVR_GetButtonAnimDataFunction(self,x):
    
    fcurve_info = {}
    fcurve_modifier_info = []
    fcurve_keyframe_point_info = []

    fcurve_info["extrapolation"] = x.extrapolation
    
    # Get Modifiers
    if x.modifiers.values() != []:
        for y in x.modifiers.values():
          
            # Clear List (.clear() -> instance problem!)
            fcurve_modifier_info_detail = {}
            
            fcurve_modifier_info_detail["type"] = y.type
            if y.type == "NOISE":
                fcurve_modifier_info_detail["scale"] = y.scale
                fcurve_modifier_info_detail["phase"] = y.phase
                fcurve_modifier_info_detail["strength"] = y.strength
                fcurve_modifier_info_detail["depth"] = y.depth
                fcurve_modifier_info_detail["offset"] = y.offset
            
            elif y.type == "CYCLES":
                fcurve_modifier_info_detail["mode_before"] = y.mode_before
                fcurve_modifier_info_detail["mode_after"] = y.mode_after
                fcurve_modifier_info_detail["cycles_before"] = y.cycles_before
                fcurve_modifier_info_detail["cycles_after"] = y.cycles_after
            
            elif y.type == "LIMITS":
                fcurve_modifier_info_detail["use_max_x"] = y.use_max_x
                fcurve_modifier_info_detail["use_max_y"] = y.use_max_y
                fcurve_modifier_info_detail["use_min_x"] = y.use_min_x
                fcurve_modifier_info_detail["use_min_y"] = y.use_min_y
                fcurve_modifier_info_detail["max_x"] = y.max_x
                fcurve_modifier_info_detail["max_y"] = y.max_y
                fcurve_modifier_info_detail["min_x"] = y.min_x
                fcurve_modifier_info_detail["min_y"] = y.min_y
            
            elif y.type == "GENERATOR":
                fcurve_modifier_info_detail["mode"] = y.mode
                fcurve_modifier_info_detail["use_additive"] = y.use_additive
                fcurve_modifier_info_detail["poly_order"] = y.poly_order
                for z in range(y.poly_order + 1):
                    fcurve_modifier_info_detail["coefficients[" + str(z) + "]"] = y.coefficients[z]
            
            elif y.type == "STEPPED":
                fcurve_modifier_info_detail["frame_step"] = y.frame_step
                fcurve_modifier_info_detail["frame_offset"] = y.frame_offset
                fcurve_modifier_info_detail["use_frame_start"] = y.use_frame_start
                fcurve_modifier_info_detail["use_frame_end"] = y.use_frame_end
                fcurve_modifier_info_detail["frame_start"] = y.frame_start
                fcurve_modifier_info_detail["frame_end"] = y.frame_end
            
            elif y.type == "FNGENERATOR":
                fcurve_modifier_info_detail["function_type"] = y.function_type
                fcurve_modifier_info_detail["use_additive"] = y.use_additive
                fcurve_modifier_info_detail["amplitude"] = y.amplitude
                fcurve_modifier_info_detail["phase_multiplier"] = y.phase_multiplier
                fcurve_modifier_info_detail["phase_offset"] = y.phase_offset
                fcurve_modifier_info_detail["value_offset"] = y.value_offset
                
            elif y.type == "ENVELOPE":
                fcurve_modifier_info_detail["reference_value"] = y.reference_value
                fcurve_modifier_info_detail["default_min"] = y.default_min
                fcurve_modifier_info_detail["default_max"] = y.default_max
                fcurve_modifier_info_detail["control_points"] = len(y.control_points)
                for z in range(len(y.control_points)):
                    fcurve_modifier_info_detail["control_points[" + str(z) + "].frame"] = y.control_points[z].frame
                    fcurve_modifier_info_detail["control_points[" + str(z) + "].min"] = y.control_points[z].min
                    fcurve_modifier_info_detail["control_points[" + str(z) + "].max"] = y.control_points[z].max
                
            fcurve_modifier_info_detail["frame_start"] = y.frame_start
            fcurve_modifier_info_detail["frame_end"] = y.frame_end
            fcurve_modifier_info_detail["blend_in"] = y.blend_in
            fcurve_modifier_info_detail["blend_out"] = y.blend_out
            fcurve_modifier_info_detail["use_influence"] = y.use_influence
            fcurve_modifier_info_detail["influence"] = y.influence
            
            fcurve_modifier_info.append(fcurve_modifier_info_detail)
            
        fcurve_info["modifiers"] = fcurve_modifier_info
    
    else:
        fcurve_info["modifiers"] = []
                 
    # Get Keyframe Points
    if x.keyframe_points.values() != []:
        for y in x.keyframe_points.values():
            
            # Clear List (.clear() -> instance problem!)
            fcurve_keyframe_point_info_detail = {}

            fcurve_keyframe_point_info_detail["interpolation"] = y.interpolation      
            fcurve_keyframe_point_info_detail["easing"] = y.easing       
            fcurve_keyframe_point_info_detail["co.x"] = y.co.x
            fcurve_keyframe_point_info_detail["co.y"] = y.co.y
            fcurve_keyframe_point_info_detail["handle_left_type"] = y.handle_left_type
            fcurve_keyframe_point_info_detail["handle_left.x"] = y.handle_left.x
            fcurve_keyframe_point_info_detail["handle_left.y"] = y.handle_left.y
            fcurve_keyframe_point_info_detail["handle_right_type"] = y.handle_right_type
            fcurve_keyframe_point_info_detail["handle_right.x"] = y.handle_right.x
            fcurve_keyframe_point_info_detail["handle_right.y"] = y.handle_right.y

            fcurve_keyframe_point_info.append(fcurve_keyframe_point_info_detail)
            
        fcurve_info["keyframe_points"] = fcurve_keyframe_point_info
    
    else:
        fcurve_info["keyframe_points"] = []
    
    return fcurve_info

def OVR_AddDriverFunction(self,DriverIndex):
  
    # Globals
    global DriverList_GlobalList
    global Driver_Settings_RL
    global Driver_Settings_Default
    # Variables
    scene_name = bpy.context.scene.name
    frame_current = bpy.context.scene.frame_current
    button_has_AnimData = False
    fcurve_AnimData = {}
    if DriverIndex == -1:
        fcurve_Compare_DriverIndex = 0
    else:
        fcurve_Compare_DriverIndex = DriverIndex
    
    # Check if Button has animation
    if self.driver_obase_path.animation_data != None:
        if self.driver_obase_path.animation_data.action != None:
            for x in self.driver_obase_path.animation_data.action.fcurves:
                if x.data_path == self.driver_orna_path and x.array_index == fcurve_Compare_DriverIndex:
                    button_has_AnimData = True
                    fcurve_AnimData = OVR_GetButtonAnimDataFunction(self,x)
                    self.driver_obase_path.animation_data.action.fcurves.remove(x)
                    break
            
    # Add Driver
    self.driver = self.driver_base_path.driver_add(self.driver_rna_path,DriverIndex)
    
    if type(self.driver.driver.expression) == "<class 'str'>" or self.driver.driver.expression == "":  
        self.driver.keyframe_points.insert(frame_current,1)
        Driver_Expression_Safe = 1
    else:
        Insert_Driver_Keyframe_Value = self.driver.driver.expression
        # Round Value if Input is Radian
        if str(self.driver_button_type) == "<class 'Euler'>" or str(self.driver_button_type) == "<class 'Quaternion'>":
            Round_Value = eval(self.driver.driver.expression) * 57.2957795 
            Round_Value = round(Round_Value)
            Round_Value/= 57.2957795
            Insert_Driver_Keyframe_Value = str(Round_Value)
        self.driver.keyframe_points.insert(frame_current,eval(Insert_Driver_Keyframe_Value))
        Driver_Expression_Safe = Insert_Driver_Keyframe_Value
    self.driver.driver.expression = "frame"
    bpy.data.scenes[0].OVR_DriverSettings.DriverAmount_GlobalIndex += 1
    NewIndexVar_01 = self.driver.driver.variables.new()
    NewIndexVar_01.name = "OVR_DriverAmount_GlobalIndex_" + str(bpy.data.scenes[0].OVR_DriverSettings.DriverAmount_GlobalIndex)
    NewIndexVar_01.type = "SINGLE_PROP"
    NewIndexVar_01.targets[0].id_type = self.driver_target_id_type
    NewIndexVar_01.targets[0].id = self.driver_target_id
    NewIndexVar_01.targets[0].data_path = self.driver_target_data_path
    NewIndexVar_02 = self.driver.driver.variables.new()
    NewIndexVar_02.name = "OVR_DriverDefault_False"
    Default_Modifier = self.driver.modifiers[0]
    self.driver.modifiers.remove(Default_Modifier)
    
    # Add to DriverList_GlobalList
    DriverList_GlobalList.add(self.driver)
    # Add to DriverListCollection
    item = bpy.data.scenes[scene_name].OVR_DriverListCollection.add()
    item.driver_id = self.driver.driver.variables[0].name[29:]
    bpy.data.scenes[scene_name].OVR_DriverListIndex = len(bpy.data.scenes[scene_name].OVR_DriverListCollection)-1

    # Add Default Values To Default List
    Driver_Settings_Default[NewIndexVar_01.name] = {}
    Driver_Settings_Default_Key = Driver_Settings_Default[NewIndexVar_01.name]
    Driver_Settings_Default_Key["variables"] = []
    Driver_Settings_Default_Key["variables"].append({})
    Driver_Settings_Default_Key["variables"][0]["name"] = "OVR_DriverDefault_True"
    Driver_Settings_Default_Key["variables"][0]["type"] = "SINGLE_PROP"
    Driver_Settings_Default_Key["variables"][0]["id_type"] = "OBJECT"
    Driver_Settings_Default_Key["variables"][0]["id"] = "None"
    Driver_Settings_Default_Key["variables"][0]["data_path"] = ""
    
    if button_has_AnimData == True:
        Driver_Settings_Default_Key["expression"] = 'frame'
        Driver_Settings_Default_Key["extrapolation"] = fcurve_AnimData["extrapolation"] 
        Driver_Settings_Default_Key["modifiers"] = fcurve_AnimData["modifiers"] 
        Driver_Settings_Default_Key["keyframe_points"] = fcurve_AnimData["keyframe_points"] 
    else:
        Driver_Settings_Default_Key["expression"] = Driver_Expression_Safe
        Driver_Settings_Default_Key["extrapolation"] = 'CONSTANT'
        Driver_Settings_Default_Key["modifiers"] = []
        Driver_Settings_Default_Key["keyframe_points"] = []
    
    # Add Default Keys to all other RenderLayers
    for x in range(len(bpy.data.scenes[scene_name].render.layers.values())-1):
        if x != bpy.data.scenes[scene_name].render.layers.active_index: 
            Driver_Settings_RL[x][NewIndexVar_01.name] = {}
            Driver_Settings_RL[x][NewIndexVar_01.name]["variables"] = []
            Driver_Settings_RL[x][NewIndexVar_01.name]["variables"].append({})
            Driver_Settings_RL[x][NewIndexVar_01.name]["variables"][0]["name"] = "OVR_DriverDefault_True" 
        else:
            Driver_Settings_RL[x][NewIndexVar_01.name] = {}
            Driver_Settings_RL[x][NewIndexVar_01.name]["variables"] = []
            Driver_Settings_RL[x][NewIndexVar_01.name]["variables"].append({})
            Driver_Settings_RL[x][NewIndexVar_01.name]["variables"][0]["name"] = "OVR_DriverDefault_False" 
            
    # NAP Generate Link 
    if self.NAP_driver_original_rna_path != None: # Execute only for NAP Properties
        # Re-Generate Properties
        global NAP_AcceptDict
        rna_path = self.NAP_driver_original_rna_path 
        id_type = self.driver_target_id_type.lower().title()
        temp_prop_name = NAP_AcceptDict[rna_path][NAP_AcceptDict[rna_path].rfind(",")+1:]
        # Generate Frame Handler
        c_prop_name = temp_prop_name
        prop_name = rna_path
        permanent_function = NAP_Frame_Change_Post_Handler_FunctionBuilder(self.driver_obase_path, c_prop_name, prop_name, NewIndexVar_01.name)
        bpy.app.handlers.frame_change_post.append(permanent_function)
        # Generate Warning Button
        id_block = id_type.lower()+"s"
        temp_panel_name = NAP_AcceptDict[rna_path][:NAP_AcceptDict[rna_path].rfind(",")]
        warning_panel_function = NAP_Warning_Panel_FunctionBuilder(temp_prop_name,id_block)
        if warning_panel_function != None:
            exec("bpy.types."+temp_panel_name+".prepend(warning_panel_function)")
            
def OVR_RemoveDriverFunction(self,DriverIndex):
  
    # Globals
    global DriverList_GlobalList
    global Driver_Settings_RL
    global Driver_Settings_Default
    # Variables
    scene_name = bpy.context.scene.name
    dirver_ID = self.driver.driver.variables[0].name

    if self.Input_Driver_Action == 'RD':
        
        # Remove from Driver_Settings_RL
        for x in range(len(bpy.data.scenes[scene_name].render.layers.values())-1):
            del Driver_Settings_RL[x][self.driver.driver.variables[0].name]
        # Remove from Driver_Settings_Default
        del Driver_Settings_Default[self.driver.driver.variables[0].name]
        # Remove from DriverList_GlobalList
        DriverList_GlobalList.remove(self.driver)
        # Remove from DriverListCollection
        bpy.data.scenes[scene_name].OVR_DriverListIndex = 0
        OVR_DriverListCollection_Loop_Counter = 0
        for x in bpy.data.scenes[scene_name].OVR_DriverListCollection:
            if x.driver_id == self.driver.driver.variables[0].name[29:]:
                OVR_DriverListCollection_RemoveIndex = OVR_DriverListCollection_Loop_Counter
                break
            else:
                OVR_DriverListCollection_Loop_Counter += 1           
        bpy.data.scenes[scene_name].OVR_DriverListCollection.remove(OVR_DriverListCollection_RemoveIndex)
        # Delete Driver
        self.driver_base_path.driver_remove(self.driver_rna_path,DriverIndex)
        
        # NAP Link Remove 
            # Re-Generate Properties
        global NAP_AcceptDict
            # Must get value though 'NAP_dict_value_search' way in order for Driver List "Remove Driver" to work
        rna_path = self.NAP_driver_original_rna_path
        if rna_path != None: # Execute only for NAP Properties
            # Set Default Value & Remove Property -> be careful not to call frame update for rest of this sub-function, since property may be deleted
            NAP_Property_Delete(self) 
            # Remove Frame Handler
            global NAP_Frame_Handler_Functions
            for x in bpy.app.handlers.frame_change_post:
                if x.__name__ == dirver_ID:
                    bpy.app.handlers.frame_change_post.remove(x)
                    NAP_Frame_Handler_Functions.remove(x)
                    break
            # Remove Warning Button -> Only Remove if all ID_Blocks don't have an OVERRIDE
            NAP_OVERRIDE_Count = 0
            for x in bpy.data.path_resolve(self.driver_target_id_type.lower()+"s"):
                if NAP_OVERRIDE_Count < 1:
                    x.animation_data_create()
                    if x.animation_data.drivers.values() != []:
                        for y in x.animation_data.drivers.values():
                            if y.data_path == self.driver_rna_path:
                                NAP_OVERRIDE_Count += 1
                else:
                    break
            if NAP_OVERRIDE_Count < 1:
                global NAP_Warning_Panel_Functions
                NAP_Warning_Panel_Function = None
                for x in NAP_Warning_Panel_Functions:
                    if x.__name__ == self.driver_rna_path:
                        NAP_Warning_Panel_Function = x
                        NAP_Warning_Panel_Functions.remove(x)
                        break
                temp_panel_name = NAP_AcceptDict[rna_path][:NAP_AcceptDict[rna_path].rfind(",")]
                exec("bpy.types."+temp_panel_name+".remove(NAP_Warning_Panel_Function)")
        
def OVR_InsertDriverValuesFunction(self):  
  
    self.driver.driver.variables[1].name = "OVR_DriverDefault_False"
    if self.Input_Driver_Keyframe_Insert == True:
        if str(self.driver_button_type) == "<class 'Euler'>" or str(self.driver_button_type) == "<class 'Quaternion'>":
            Insert_Driver_Keyframe_Value = self.Input_Driver_Keyframe_Value / 57.2957795 # Convert Degree to Radian
            self.driver.keyframe_points.insert(bpy.context.scene.frame_current,Insert_Driver_Keyframe_Value)  
        else:
            self.driver.keyframe_points.insert(bpy.context.scene.frame_current,self.Input_Driver_Keyframe_Value)
    if self.Input_Driver_Expression_Insert == True:
        self.driver.driver.expression =  self.Input_Driver_Expression_String
    # Refresh Scene
    bpy.context.scene.update()
        
def OVR_RemoveDriverValueFunction(self):  
      
    if self.Input_Driver_Action == 'RK':
        for i in range(len(self.driver.keyframe_points)):
            if self.driver.keyframe_points[i].co.x == bpy.context.scene.frame_current: 
                self.driver.keyframe_points.remove(self.driver.keyframe_points[i])
                self.report({'INFO'}, "OVR | Keyframes deleted at this Frame.")
                bpy.context.scene.frame_set(bpy.context.scene.frame_current) # Force Update
                return
        self.report({'WARNING'}, "OVR | No Keyframes found at this Frame.")
        
        
def OVR_SetToDefaultDriverFunction(self):  
  
    # Variables
    scene_name = bpy.context.scene.name
      
    if self.Input_Driver_Action == 'SD':
        self.driver.driver.variables[1].name = "OVR_DriverDefault_True"
        bpy.data.scenes[scene_name].OVR_ActiveRenderLayerIndex = bpy.data.scenes[scene_name].render.layers.active_index           

# Add / Update Driver  
class OVR_AddDriver(bpy.types.Operator):
    bl_idname = "ovr.adddriver"
    bl_label = "Add OVR Driver"
    bl_description = "Add an OVR Driver"
    bl_options = {"REGISTER"}
    
    # Class Variables
    dependency_graph_hack_base_path  = None
    driver = None
    DriverExists = False
    driver_obase_path = None
    driver_orna_path = None
    driver_base_path = None
    driver_rna_path = None
    driver_button_type = None
    driver_target_id_type = None
    driver_target_id = None
    driver_target_data_path = None
    driver_isVector = False
    driver_isVector_DriverList = []
    driver_isVector_ArrayIndex = []
    driver_isVector_NormalDriverSlots = []
    driver_Info_OverrideCurrenLayerExist = {}
    driver_Info_OverrideCurrenLayerExist_Bool = False
    NAP_driver_original_rna_path = None

    # User Input Variables
    Input_Driver_Expression_Insert = BoolProperty(name='Insert Driver Expression', default = True, description ='Enable Driver Expression')
    Input_Driver_Expression_String = StringProperty(name="      Expression", default="frame", description ='Enter Driver Expression')
    Input_Driver_Keyframe_Insert = BoolProperty(name='Insert Keyframe', default = False, description ='Enable Insert Keyframe')
    Input_Driver_Keyframe_Value = FloatProperty(name='Value', default = 0, description ='Enter Keyframe Value')
    Input_Driver_Vector_All = BoolProperty(name='All List Members', default = False, description ='Enable Insert Keyframe for whole List/Vector')
    Input_Driver_Vector_Number = IntProperty(name='Slot Number', default = 1, min = 1, max = 3,  description ='Enter List Item Slot Number')
    
    def draw(self, context):
      
        layout = self.layout
        col = layout.column()
        
        col.prop(self, 'Input_Driver_Expression_Insert')
        col.prop(self, 'Input_Driver_Expression_String')
        col.separator()
        row = col.row()
        row.prop(self, 'Input_Driver_Keyframe_Insert')
        row.prop(self, 'Input_Driver_Keyframe_Value')

        # Display Different Menu For Vectors/"Lists"
        if self.driver_isVector == True:
            col.separator()
            row = col.row()
            row.prop(self, 'Input_Driver_Vector_All')
            row.prop(self, 'Input_Driver_Vector_Number')
            col.separator()
        
        if self.driver_Info_OverrideCurrenLayerExist != {} or self.driver_isVector_NormalDriverSlots != []:
            col.label(text="----------------------------------------------------------------------------------------------")
        
        # Display Label with Info about Driver State"
        if self.driver_Info_OverrideCurrenLayerExist != {}: 
            row = col.row()
            row.label(text="Driver State for active RL ")
            row.label(text=str(self.driver_Info_OverrideCurrenLayerExist)[1:-1])
            
        # Display Label with Info about Vector/"List"
        if self.driver_isVector_NormalDriverSlots != []:
            row = col.row()
            row.label(text="'Normal' Driver at Slot")
            row.label(text=str(self.driver_isVector_NormalDriverSlots)[1:-1])
                       
        col.separator()
         
    def invoke(self, context, event) :
      
        # Reset Class Variables
        self.dependency_graph_hack_base_path = None
        self.driver = None
        self.DriverExists = False
        self.driver_obase_path = None
        self.driver_orna_path = None
        self.driver_base_path = None
        self.driver_rna_path = None
        self.driver_button_type = None
        self.driver_target_id_type = None
        self.driver_target_id = None
        self.driver_target_data_path = None
        self.driver_isVector = False
        self.driver_isVector_DriverList = []
        self.driver_isVector_ArrayIndex = []
        self.driver_isVector_NormalDriverSlots = []
        self.driver_Info_OverrideCurrenLayerExist = {}
        self.driver_Info_OverrideCurrenLayerExist_Bool = False
        self.NAP_driver_original_rna_path = None
           
        # Get Data / If return == False Context of Operator is Wrong or Property is not Animatable 
        if OVR_GetButtonDataFunction(self) == False:
            return {"CANCELLED"}
        
        # Dependency Graph Hack 
        if self.dependency_graph_hack_base_path != None:
            self.dependency_graph_hack_base_path.use_extra_recalc_data = True
            
        # Check if Driver Exists
        if self.driver_obase_path.animation_data != None:
            if self.driver_obase_path.animation_data.drivers.values() != []:
                driver_amount = len(self.driver_obase_path.animation_data.drivers)
                for x in range(driver_amount):
                    if self.driver_obase_path.animation_data.drivers[x].data_path == self.driver_orna_path:
                        if self.driver_isVector == True:
                            driver_temporary = None
                            driver_temporary = self.driver_obase_path.animation_data.drivers[x]
                            array_index_temporary = driver_temporary.array_index+1
                            self.driver_isVector_ArrayIndex.append(driver_temporary.array_index)
                            self.driver_isVector_DriverList.append(driver_temporary)
                            if len(driver_temporary.driver.variables.values()) == 0:
                                self.driver_isVector_NormalDriverSlots.append(array_index_temporary)                               
                                continue
                            elif str(driver_temporary.driver.variables[0].name)[:10] != "OVR_Driver":  
                                self.driver_isVector_NormalDriverSlots.append(array_index_temporary)
                                continue
                            if driver_temporary.driver.variables[1].name == "OVR_DriverDefault_True":
                                self.driver_Info_OverrideCurrenLayerExist[array_index_temporary] = "DF"
                            else:
                                self.driver_Info_OverrideCurrenLayerExist[array_index_temporary] = "OVR" 
                        else:
                            self.driver = self.driver_obase_path.animation_data.drivers[x]
                            if len(self.driver.driver.variables.values()) == 0:
                                self.report({'WARNING'}, "OVR | This button has a 'normal' Driver")
                                NAP_Property_Delete(self)
                                return {"CANCELLED"}
                            elif str(self.driver.driver.variables[0].name)[:10] != "OVR_Driver":  
                                self.report({'WARNING'}, "OVR | This button has a 'normal' Driver")
                                NAP_Property_Delete(self)
                                return {"CANCELLED"}
                            self.DriverExists = True
                            if self.driver.driver.variables[1].name == "OVR_DriverDefault_True":
                                self.driver_Info_OverrideCurrenLayerExist[1] = "DF"
                            else:
                                self.driver_Info_OverrideCurrenLayerExist[1] = "OVR" 
                                self.driver_Info_OverrideCurrenLayerExist_Bool = True
                            break  
                    # Data Evaluate
                    if len(self.driver_isVector_ArrayIndex) == 3:
                        self.DriverExists = True
                        self.driver_Info_OverrideCurrenLayerExist_Bool = True
                        break
                                  
        # Set/Revert Dialog Box Defaults
        if self.DriverExists == True and self.driver_Info_OverrideCurrenLayerExist_Bool == True: 
          self.Input_Driver_Expression_Insert = False
          self.Input_Driver_Keyframe_Insert = True
        else:
          self.Input_Driver_Expression_Insert = True
          self.Input_Driver_Keyframe_Insert = False
        self.Input_Driver_Expression_String = "frame"  
        self.Input_Driver_Keyframe_Value = 0 
        self.Input_Driver_Vector_All = False
        self.Input_Driver_Vector_Number = 1

        return context.window_manager.invoke_props_dialog(self)
    
    def cancel(self,context):
        NAP_Property_Delete(self)
        return 
        
    def execute(self, context): 
      
        # Variables
        driver_isVector_NormalDriverSlots = []
      
        # Operator Variables
        self.Input_Driver_Vector_Number -=1
        
        if self.driver_isVector == True:
            if self.Input_Driver_Vector_All == True:
                for x in range(0,3):
                    if x in self.driver_isVector_ArrayIndex:
                        for i in self.driver_isVector_DriverList:
                            if i.array_index == x:
                                self.driver = i
                                if len(self.driver.driver.variables.values()) == 0:
                                    driver_isVector_NormalDriverSlots.append(x+1)
                                    continue
                                elif str(self.driver.driver.variables[0].name)[:10] != "OVR_Driver":  
                                    driver_isVector_NormalDriverSlots.append(x+1)
                                    continue
                                OVR_InsertDriverValuesFunction(self)
                    else:
                        if bpy.data.scenes[bpy.context.scene.name].render.layers.active.name == "OVR_Default_RenderLayer":
                            self.report({'WARNING'}, "OVR | Can't create Overrides on 'OVR_Default_RenderLayer' Layer")
                            continue
                        OVR_AddDriverFunction(self,x)
                        OVR_InsertDriverValuesFunction(self)
                if len(driver_isVector_NormalDriverSlots) != 0:
                    self.report({'INFO'}, "OVR | Skipped 'normal' Driver at Slot "+str(driver_isVector_NormalDriverSlots)[1:-1]+" in this Vector/'List'")
            else:
                if self.Input_Driver_Vector_Number in self.driver_isVector_ArrayIndex:
                    for i in self.driver_isVector_DriverList:
                            if i.array_index == self.Input_Driver_Vector_Number:
                                self.driver = i
                                if len(self.driver.driver.variables.values()) == 0:
                                    self.report({'WARNING'}, "OVR | This Vector/'List' slot has a 'normal' Driver")
                                    return {"CANCELLED"} 
                                elif str(self.driver.driver.variables[0].name)[:10] != "OVR_Driver":  
                                    self.report({'WARNING'}, "OVR | This Vector/'List' slot has a 'normal' Driver")
                                    return {"CANCELLED"} 
                                OVR_InsertDriverValuesFunction(self)
                else:
                    if bpy.data.scenes[bpy.context.scene.name].render.layers.active.name == "OVR_Default_RenderLayer":
                        self.report({'WARNING'}, "OVR | Can't create Overrides on 'OVR_Default_RenderLayer' Layer")
                        return {"CANCELLED"} 
                    OVR_AddDriverFunction(self,self.Input_Driver_Vector_Number)
                    OVR_InsertDriverValuesFunction(self)
        else:                       
            # Add Driver and Reset to Default
            if self.DriverExists == False:    
                if bpy.data.scenes[bpy.context.scene.name].render.layers.active.name == "OVR_Default_RenderLayer":
                        self.report({'WARNING'}, "OVR | Can't create Overrides on 'OVR_Default_RenderLayer' Render Layer")
                        return {"CANCELLED"} 
                OVR_AddDriverFunction(self,-1)
             
            # Insert Driver Values  
            OVR_InsertDriverValuesFunction(self)
        
        return {"FINISHED"}
      
# Remove Driver
class OVR_RemoveDriver(bpy.types.Operator):
    bl_idname = "ovr.removedriver"
    bl_label = "Remove OVR Driver"
    bl_description = "Remove an OVR Driver"
    bl_options = {"REGISTER"}
    
    # Class Variables
    driver = None
    DriverExists = False
    driver_obase_path = None
    driver_orna_path = None
    driver_base_path = None
    driver_rna_path = None
    driver_button_type = None
    driver_target_id_type = None
    driver_target_id = None
    driver_target_data_path = None
    driver_isVector = False
    driver_isVector_DriverList = []
    driver_isVector_ArrayIndex = []
    driver_isVector_NormalDriverSlots = []
    driver_Info_OverrideCurrenLayerExist = {}
    NAP_driver_original_rna_path = None
    
    # User Input Variables
    Input_Driver_Action_Items = [
        ('RK', "Remove Keyframe", "Remove Keyframe"),
        ('SD', "Set to Default Driver", "Set to Default Driver"),
        ('RD', "Remove Driver", "Remove Driver"),
    ]
    Input_Driver_Action = EnumProperty(items=Input_Driver_Action_Items,name='', default = 'RK')

    Input_Driver_Vector_All = BoolProperty(name='All List Members', default = False, description ='Enable Remove Keyframe for whole List/Vector')
    Input_Driver_Vector_Number = IntProperty(name='Slot Number', default = 1, min = 1, max = 3,  description ='Enter List Item Slot Number')
    
    def draw(self, context):
      
        layout = self.layout
        col = layout.column()

        row = col.row()
        row.prop(self,"Input_Driver_Action")
        
        #Display Different Menu For Vectors/"Lists"
        if self.driver_isVector == True:
            col.separator()
            row = col.row()
            row.prop(self, 'Input_Driver_Vector_All')
            row.prop(self, 'Input_Driver_Vector_Number')
           
        if self.driver_Info_OverrideCurrenLayerExist != {} or self.driver_isVector_NormalDriverSlots != []:
            col.label(text="----------------------------------------------------------------------------------------------")
        
        # Display Label with Info about Driver State"
        if self.driver_Info_OverrideCurrenLayerExist != {}: 
            row = col.row()
            row.label(text="Driver State for active RL ")
            row.label(text=str(self.driver_Info_OverrideCurrenLayerExist)[1:-1])
            
        # Display Label with Info about Vector/"List"
        if self.driver_isVector_NormalDriverSlots != []:
            row = col.row()
            row.label(text="'Normal' Driver at Slot")
            row.label(text=str(self.driver_isVector_NormalDriverSlots)[1:-1])
       
        col.separator()
         
    def invoke(self, context, event) :
      
        # Reset Class Variables
        self.driver = None
        self.DriverExists = False
        self.driver_obase_path = None
        self.driver_orna_path = None
        self.driver_base_path = None
        self.driver_rna_path = None
        self.driver_button_type = None
        self.driver_target_id_type = None
        self.driver_target_id = None
        self.driver_target_data_path = None
        self.driver_isVector = False
        self.driver_isVector_DriverList = []
        self.driver_isVector_ArrayIndex = []
        self.driver_isVector_NormalDriverSlots = []
        self.driver_Info_OverrideCurrenLayerExist = {}
        self.NAP_driver_original_rna_path = None
      
        # Get Data / If return == False Context of Operator is Wrong or Property is not Animatable 
        if OVR_GetButtonDataFunction(self) == False:
            return {"CANCELLED"}
        
        # Get Driver / Check if Driver Exists
        if self.driver_obase_path.animation_data != None:
            if self.driver_obase_path.animation_data.drivers.values() != []:
                driver_amount = len(self.driver_obase_path.animation_data.drivers)
                for x in range(driver_amount):
                    if self.driver_obase_path.animation_data.drivers[x].data_path == self.driver_orna_path:
                        self.DriverExists = True
                        if self.driver_isVector == True:
                            driver_temporary = None
                            driver_temporary = self.driver_obase_path.animation_data.drivers[x]
                            array_index_temporary = driver_temporary.array_index+1
                            self.driver_isVector_ArrayIndex.append(self.driver_obase_path.animation_data.drivers[x].array_index)
                            self.driver_isVector_DriverList.append(self.driver_obase_path.animation_data.drivers[x])
                            if len(driver_temporary.driver.variables.values()) == 0:
                                self.driver_isVector_NormalDriverSlots.append(array_index_temporary)                               
                                continue
                            elif str(driver_temporary.driver.variables[0].name)[:10] != "OVR_Driver":  
                                self.driver_isVector_NormalDriverSlots.append(array_index_temporary)
                                continue
                            if driver_temporary.driver.variables[1].name == "OVR_DriverDefault_True":
                                self.driver_Info_OverrideCurrenLayerExist[array_index_temporary] = "DF"
                            else:
                                self.driver_Info_OverrideCurrenLayerExist[array_index_temporary] = "OVR" 
                        else:
                            self.driver = self.driver_obase_path.animation_data.drivers[x]
                            if len(self.driver.driver.variables.values()) == 0:
                                self.report({'WARNING'}, "OVR | This button has a 'normal' Driver")
                                return {"CANCELLED"}
                            elif str(self.driver.driver.variables[0].name)[:10] != "OVR_Driver":                             
                                self.report({'WARNING'}, "OVR | This button has a 'normal' Driver")
                                return {"CANCELLED"}
                            if self.driver.driver.variables[1].name == "OVR_DriverDefault_True":
                                self.driver_Info_OverrideCurrenLayerExist[1] = "DF"
                            else:
                                self.driver_Info_OverrideCurrenLayerExist[1] = "OVR" 
                            break 
                    # Data Evaluate
                    if len(self.driver_isVector_ArrayIndex) == 3:
                        break 
        if self.DriverExists == False:
            self.report({'WARNING'}, "OVR | This button doesn't have a driver")
            return {"CANCELLED"}
                                  
        # Set/Revert Dialog Box Defaults
        self.Input_Driver_Action = 'RK'
        self.Input_Driver_Vector_All = False
        self.Input_Driver_Vector_Number = 1

        return context.window_manager.invoke_props_dialog(self)

    def execute(self, context): 
        
        # Variables
        driver_isVector_NormalDriverSlots = []
        
        # Operator Variables
        self.Input_Driver_Vector_Number -=1
        
        if self.driver_isVector == True:
            if self.Input_Driver_Vector_All == True:
                for x in range(0,3):
                    if x in self.driver_isVector_ArrayIndex:                                            
                        for i in self.driver_isVector_DriverList:
                            if i.array_index == x:
                                self.driver = i
                                if len(self.driver.driver.variables.values()) == 0:
                                    driver_isVector_NormalDriverSlots.append(x+1)
                                    continue
                                elif str(self.driver.driver.variables[0].name)[:10] != "OVR_Driver":  
                                    driver_isVector_NormalDriverSlots.append(x+1)
                                    continue
                                OVR_RemoveDriverValueFunction(self) 
                                OVR_SetToDefaultDriverFunction(self)
                                OVR_RemoveDriverFunction(self,x)   
                if len(driver_isVector_NormalDriverSlots) != 0:
                    self.report({'INFO'}, "OVR | Skipped 'normal' Driver at Slot "+str(driver_isVector_NormalDriverSlots)[1:-1]+" in this Vector/'List'")
            else:
                if self.Input_Driver_Vector_Number in self.driver_isVector_ArrayIndex:                        
                    for i in self.driver_isVector_DriverList:
                            if i.array_index == self.Input_Driver_Vector_Number:
                                self.driver = i
                                if len(self.driver.driver.variables.values()) == 0:
                                    self.report({'WARNING'}, "OVR | This button has a 'normal' Driver")
                                    return {"CANCELLED"} 
                                elif str(self.driver.driver.variables[0].name)[:10] != "OVR_Driver":  
                                    self.report({'WARNING'}, "OVR | This button has a 'normal' Driver")
                                    return {"CANCELLED"} 
                                OVR_RemoveDriverValueFunction(self) 
                                OVR_SetToDefaultDriverFunction(self)
                                OVR_RemoveDriverFunction(self,self.Input_Driver_Vector_Number)
                else:
                    self.report({'WARNING'}, "OVR | This button doesn't have a driver")
                    return {"CANCELLED"} 
        else:                       
            OVR_RemoveDriverValueFunction(self) 
            OVR_SetToDefaultDriverFunction(self)
            OVR_RemoveDriverFunction(self,-1)
    
        return {"FINISHED"}
  
############################################################################################################### Driver Functions

############################################################################################################### Driver List Functions

class OVR_InsertKeyframe_ListButton(bpy.types.Operator):
    bl_idname = "ovr.insertkeyframe_listbutton"
    bl_label = "List Button | Insert Keyframe"
    bl_options = {"REGISTER"}
    
    # Class Variables
    driver = None
    driver_button_type = None
    driver_Info_OverrideCurrenLayerExist = {}
    driver_Info_OverrideCurrenLayerExist_Bool = False

    # User Input Variables
    Input_Driver_Expression_Insert = BoolProperty(name='Insert Driver Expression', default = True, description ='Enable Driver Expression')
    Input_Driver_Expression_String = StringProperty(name="      Expression", default="frame", description ='Enter Driver Expression')
    Input_Driver_Keyframe_Insert = BoolProperty(name='Insert Keyframe', default = False, description ='Enable Insert Keyframe')
    Input_Driver_Keyframe_Value = FloatProperty(name='Keyframe Value', default = 0, description ='Enter Keyframe Value')
    
    def draw(self, context):
      
        layout = self.layout
        col = layout.column()
        
        col.prop(self, 'Input_Driver_Expression_Insert')
        col.prop(self, 'Input_Driver_Expression_String')
        col.separator()
        row = col.row()
        row.prop(self, 'Input_Driver_Keyframe_Insert')
        row.prop(self, 'Input_Driver_Keyframe_Value')

        if self.driver_Info_OverrideCurrenLayerExist != {} or self.driver_isVector_NormalDriverSlots != []:
            col.label(text="----------------------------------------------------------------------------------------------")
        
        # Display Label with Info about Driver State"
        if self.driver_Info_OverrideCurrenLayerExist != {}: 
            row = col.row()
            row.label(text="Driver State for active RL ")
            row.label(text=str(self.driver_Info_OverrideCurrenLayerExist)[1:-1])
                    
        col.separator()
         
    def invoke(self, context, event) :
      
        # Globals
        global Active_List_Driver
          
        # Reset/Set Class Variables
        self.driver = Active_List_Driver
        temp_base_path = self.driver.driver.variables[0].targets[0].id
        if self.driver.data_path[:5] == "nodes":
            temp_base_path = temp_base_path.node_tree
        self.driver_button_type = type(temp_base_path.path_resolve(self.driver.data_path)) 
        self.driver_Info_OverrideCurrenLayerExist = {}
        self.driver_Info_OverrideCurrenLayerExist_Bool = False
            
        # Check if Driver has an Override for the current RenderLayer
        if self.driver.driver.variables[1].name == "OVR_DriverDefault_True":
            self.driver_Info_OverrideCurrenLayerExist[1] = "DF"
        else:
            self.driver_Info_OverrideCurrenLayerExist[1] = "OVR" 
            self.driver_Info_OverrideCurrenLayerExist_Bool = True

        # Set/Revert Dialog Box Defaults
        if self.driver_Info_OverrideCurrenLayerExist_Bool == True: 
          self.Input_Driver_Expression_Insert = False
          self.Input_Driver_Keyframe_Insert = True
        else:
          self.Input_Driver_Expression_Insert = True
          self.Input_Driver_Keyframe_Insert = False
        self.Input_Driver_Expression_String = "frame"
        self.Input_Driver_Keyframe_Value = 0

        return context.window_manager.invoke_props_dialog(self)

    def execute(self, context):                       
         
        # Insert Driver Values  
        OVR_InsertDriverValuesFunction(self)
        
        return {"FINISHED"}

class OVR_RemoveKeyframe_ListButton(bpy.types.Operator):
    bl_idname = "ovr.removekeyframe_listbutton"
    bl_label = "List Button | Remove Keyframe"
    bl_options = {"REGISTER"}
    
    driver = None
    Input_Driver_Action = None
    
    def execute(self, context): 
        
        # Globals
        global Active_List_Driver
          
        # Set Class Variables
        self.driver = Active_List_Driver
        self.Input_Driver_Action = "RK"
            
        OVR_RemoveDriverValueFunction(self)  
    
        return {"FINISHED"}

class OVR_SetToDefault_ListButton(bpy.types.Operator):
    bl_idname = "ovr.settodefault_listbutton"
    bl_label = "List Button | Set Driver To Default Value"
    bl_description = "List Button | Set Driver To Default Value"
    bl_options = {"REGISTER"}
    
    driver = None
    Input_Driver_Action = None
    
    def execute(self, context): 
        
        # Globals
        global Active_List_Driver
          
        # Set Class Variables
        self.driver = Active_List_Driver
        self.Input_Driver_Action = "SD"
        
        OVR_SetToDefaultDriverFunction(self)
    
        return {"FINISHED"}

class OVR_RemoveDriver_ListButton(bpy.types.Operator):
    bl_idname = "ovr.removedriver_listbutton"
    bl_label = "List Button | Remove OVR Driver"
    bl_description = "List Button | Remove an OVR Driver"
    bl_options = {"REGISTER"}
    
    driver = None
    driver_base_path = None
    driver_rna_path = None
    Input_Driver_Action = None
    
    def execute(self, context): 

        # Globals
        global Active_List_Driver
          
        # Set Class Variables
        self.driver = Active_List_Driver
        temp_base_path = self.driver.driver.variables[0].targets[0].id
        if self.driver.data_path[:5] == "nodes":
            temp_base_path = temp_base_path.node_tree
        self.driver_base_path = temp_base_path
        self.driver_rna_path = self.driver.data_path
        self.driver_target_id_type = self.driver.driver.variables[0].targets[0].id_type
        self.Input_Driver_Action = "RD"
        self.NAP_driver_original_rna_path = NAP_dict_value_search(NAP_AcceptDict,self.driver_rna_path)
        
        # Check if Driver is an Array Driver
        temp_base_path = self.driver.driver.variables[0].targets[0].id
        if self.driver.data_path[:5] == "nodes":
            temp_base_path = temp_base_path.node_tree
        driver_button_type = type(temp_base_path.path_resolve(self.driver.data_path)) 
        if str(driver_button_type) == "<class 'Vector'>" or str(driver_button_type) == "<class 'Euler'>" or str(driver_button_type) == "<class 'Quaternion'>" or str(driver_button_type) == "<class 'Color'>" or str(driver_button_type) == "<class 'bpy_prop_array'>":
            array_index = self.driver.array_index
        else:
            array_index = -1  
            
        OVR_RemoveDriverFunction(self,array_index)  
    
        return {"FINISHED"}
  

def OVR_GenerateOverrideStateInformationFunction(driver,ShortName):
    
    # Globals
    global Driver_Settings_RL
    # Variables
    scene_name = bpy.context.scene.name
    OutputText = []
    Driver_Settings_RL_Loop_Counter = 0

    for x in Driver_Settings_RL:  
        if bpy.data.scenes[scene_name].render.layers.active_index != Driver_Settings_RL_Loop_Counter: 
            state = x[driver.driver.variables[0].name]["variables"][0]["name"]
        else:
            state = driver.driver.variables[1].name
        if state == "OVR_DriverDefault_False":
            if ShortName != True:
                OutputText.append("OVERRIDE")
            else:
                OutputText.append("O")     
        else:
            if ShortName != True:
                OutputText.append("Default")
            else:
                OutputText.append("D")
        Driver_Settings_RL_Loop_Counter += 1
        
    return OutputText
    
class OVR_DriverListSettings(bpy.types.PropertyGroup):
    driver_id = bpy.props.StringProperty()
    
class OVR_DriverList_Template(bpy.types.UIList):

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
        
        # Globals
        global DriverList_GlobalList
        global Active_List_Driver_NameNice
        # Variables
        scene = bpy.context.scene
        id_Found = False
        Item_Info = ""
        
        if item:
            for x in DriverList_GlobalList:
                if x.data_path != "":
                    if str(x.driver.variables[0].name)[29:] == item.driver_id:
                        
                    # Generate List Name/Icon
                        id_Found = True
                        dt = x.driver.variables[0].targets[0]
                        Datapath_Name = x.data_path.replace("_"," ").replace("cycles.","cycles ").replace("cycles visibility",'["cycles visibility"]').replace("cycles curves",'["cycles curves"]').replace("render.","render ").title() 
                        p_Icon, p_Partition, p_NameNice = dt.data_path.partition(',')
                        # Get Base Name
                        id_str = str(dt.id)
                        Item_Name = id_str[id_str.find('"')+1 : id_str.rfind('"')]
                        # Fix Data Path Name
                        if Datapath_Name.find(".") != -1:
                            Datapath_1 = Datapath_Name[Datapath_Name.find('"')+1 : Datapath_Name.rfind('"')]
                            Datapath_2 = Datapath_Name[Datapath_Name.rfind('"')+3 :]
                            Datapath_Name = Datapath_1 + " > " + Datapath_2
                            # Fix Node Tree Names
                            if Datapath_Name.find("Default Value") != -1:
                                Datapath_Name = Datapath_Name[:-23] + p_NameNice     
                        # Change Data Path Name for Vectors
                        if Datapath_Name.find("Location") != -1 or Datapath_Name.find("Rotation") != -1 or Datapath_Name.find("Scale") != -1 or Datapath_Name.find("Gravity") != -1 or Datapath_Name.find("Object Align Factor") != -1:
                            if x.array_index == 0:
                                Datapath_Name = Datapath_Name + " X"
                            elif x.array_index == 1:
                                Datapath_Name = Datapath_Name + " Y"
                            elif x.array_index == 2:
                                Datapath_Name = Datapath_Name + " Z"
                        if Datapath_Name.find("Color") != -1:
                            if x.array_index == 0:
                                Datapath_Name = Datapath_Name + " R"
                            elif x.array_index == 1:
                                Datapath_Name = Datapath_Name + " G"
                            elif x.array_index == 2:
                                Datapath_Name = Datapath_Name + " B"
                         
                        # Generate List OVR State Short Display Text        
                        OverrideStates = OVR_GenerateOverrideStateInformationFunction(x,True) 
                        OverrideStates_Loop_Counter = 1
                        for x in OverrideStates:
                            Item_Info = Item_Info + str(OverrideStates_Loop_Counter) + ":" + x + "|"
                            OverrideStates_Loop_Counter += 1
                        Item_Info = Item_Info[:-1]
                        
                        Item_Name = Item_Name + " | " + Datapath_Name   
                        Item_Icon = p_Icon

                        # Get NameNice    
                        active_driver_id = scene.OVR_DriverListCollection[scene.OVR_DriverListIndex].driver_id
                        if item.driver_id == active_driver_id:
                            Active_List_Driver_NameNice = Datapath_Name

                        break
            if id_Found == False:
                Item_Name = "Refresh Render Layers to Delete Item" 
                Item_Icon = "INFO"
                Item_Info = ""
                Datapath_Name = "None"
                                       
            split = layout.split(percentage=0.65)
            # Driver Name Display
            split.label(text=Item_Name,translate=False,icon=Item_Icon)
            # Driver OVR States Short Display
            split.label(text=Item_Info,translate=False) 

class OVR_DriverList(bpy.types.Panel):
    
    bl_idname = "ovr.driver_list"
    bl_label = "OVERRIDE Driver List"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render_layer"
    
    def draw(self, context):
        
        # Globals
        global DriverList_GlobalList
        global Active_List_Driver
        global Active_List_Driver_NameNice 

        # Variables
        layout = self.layout
        scene_name = bpy.context.scene.name
        scene = bpy.data.scenes[scene_name]
        Active_List_Driver_Found = False
        
        # List Display
        layout.template_list("OVR_DriverList_Template","", scene, "OVR_DriverListCollection", scene, "OVR_DriverListIndex") 
        
        # List Item Display
        if scene.OVR_DriverListCollection.values() != []:                          
            driver_id = scene.OVR_DriverListCollection[scene.OVR_DriverListIndex].driver_id
            # Get Active Driver
            for x in DriverList_GlobalList:
                if x.data_path != "":
                    if str(x.driver.variables[0].name)[29:] == driver_id:
                        Active_List_Driver = x
                        Active_List_Driver_Found = True
                        break
            
            if Active_List_Driver_Found == True:         
                
                # Quick Operator Access Execution Interface
                row = layout.row()
                row_left = row.column(align=True) 
                row_left.operator("ovr.insertkeyframe_listbutton", text="Insert Keyframe",icon='KEY_HLT')
                row_left.operator("ovr.removekeyframe_listbutton", text="Remove Keyframe",icon='KEY_DEHLT')
                row_right = row.column(align=True) 
                row_right.operator("ovr.settodefault_listbutton", text="Set Driver To Default Value",icon='NDOF_TURN')
                row_right.operator("ovr.removedriver_listbutton", text="Remove Driver", icon='CANCEL')
                
                # Generate Information Interface   
                layout.separator() 
                row = layout.row()
                row.label(text="Driver's State per RenderLayer",translate=False) 
                
                split = layout.split(percentage = 0.35) 
                col_left = split.column()       
                col_right = split.column()   
                OverrideStates = OVR_GenerateOverrideStateInformationFunction(Active_List_Driver,False)
                OverrideStates_Loop_Counter = 0
                for x in OverrideStates:
                    Active_Render_Layer_Name = scene.render.layers[OverrideStates_Loop_Counter].name
                    col_left.label(text=Active_Render_Layer_Name,translate=False)
                    col_right.label(text=x,translate=False)
                    OverrideStates_Loop_Counter += 1
                OverrideStates.append("Default RenderLayer") 
                
                row = layout.row()
                row.label(text="Active Render Layer Information",translate=False) 
                split = layout.split(percentage = 0.35) 
                col_left = split.column()       
                col_right = split.column()  
                col_left.label(text="             State",translate=False) 
                col_right.label(text=OverrideStates[scene.render.layers.active_index],translate=False)    
                col_left.label(text="Current Value",translate=False) 
                temporary_cut_index = Active_List_Driver.data_path.rfind(".")
                temporary_idp = Active_List_Driver.data_path
                if temporary_idp.find("color") != -1 or temporary_idp.find("location") != -1 or temporary_idp.find("rotation") != -1 or temporary_idp.find("scale") != -1 or temporary_idp.find("gravity") != -1 or temporary_idp.find("object_align_factor") != -1:
                    temporary_basepath = Active_List_Driver.id_data
                    temporary_datapath = Active_List_Driver.data_path
                    temporary_index = Active_List_Driver.array_index
                elif temporary_cut_index == -1:
                    temporary_basepath = Active_List_Driver.id_data
                    temporary_datapath = Active_List_Driver.data_path
                    temporary_index = -1
                else:
                    temporary_basepath = Active_List_Driver.id_data.path_resolve(Active_List_Driver.data_path[:temporary_cut_index])
                    temporary_datapath = Active_List_Driver.data_path[temporary_cut_index+1:]
                    temporary_index = Active_List_Driver.array_index
                col_right.prop(temporary_basepath,temporary_datapath,index=temporary_index,text=Active_List_Driver_NameNice)     
                
            else:
                layout.separator() 
                row = layout.row()
                row.label(text="Refresh Render Layers to Delete Item",translate=False) 
                layout.separator() 
            
############################################################################################################### Driver List Functions

############################################################################################################### Render Layer Functions

def OVR_Clean_Lists_CollectionsFunction():
    # Globals
    global DriverList_GlobalList
    global Driver_Settings_RL
    global Driver_Settings_Default
    
    # Variables
    scene_name = bpy.context.scene.name
    driver_remove_list = []
    driver_name_list = []
    driver_remove_settings_list = set()
    driver_id_list = []
    driver_remove_collection_list = []
        
    # Clean DriverList_GlobalList > To avoid Error caused by User deleting driver manually | Not a clean solution > Cleaner : Reload whole list 
    bpy.data.scenes[scene_name].update()
    for x in DriverList_GlobalList:
        if x.data_path == "":
            driver_remove_list.append(x)  
    for x in driver_remove_list:
        DriverList_GlobalList.remove(x)
    # Clean Driver_Settings-Lists
    if str(DriverList_GlobalList) != "set()":
        for x in DriverList_GlobalList:
            driver_name_list.append(x.driver.variables[0].name)
        for x in Driver_Settings_RL:
            for y in x:
                if y not in driver_name_list:
                    driver_remove_settings_list.add(y)
        for x in Driver_Settings_RL:
            for y in driver_remove_settings_list:
                del x[y]
        for y in driver_remove_settings_list:
                del Driver_Settings_Default[y]
    # Clean DriverListCollection
    if bpy.data.scenes[scene_name].OVR_DriverListCollection != []:
        OVR_DriverListCollection_Loop_Counter = 0
        for x in DriverList_GlobalList:
            driver_id_list.append(x.driver.variables[0].name[29:])
        for x in bpy.data.scenes[scene_name].OVR_DriverListCollection:
            if x.driver_id not in driver_id_list:
                driver_remove_collection_list.append(OVR_DriverListCollection_Loop_Counter)        
            else:
                OVR_DriverListCollection_Loop_Counter += 1 
        for x in driver_remove_collection_list:
            bpy.data.scenes[scene_name].OVR_DriverListCollection.remove(x)

def OVR_CopyDriversSettingsFunction():
  
    # Globals
    global DriverList_GlobalList
    global Driver_Settings_RL
    global Driver_Settings_Default
    
    # Variables
    scene_name = bpy.context.scene.name
    if bpy.data.scenes[scene_name].render.layers.active.name == "OVR_Default_RenderLayer": # Check if User is on Default Driver RenderLayer
        for x in DriverList_GlobalList:    # Reset to OVR_DriverDefault_True -> else Default RenderLayers would become OVERRIDE RenderLayers
            x.driver.variables[1].name = "OVR_DriverDefault_True" 
        active_settings_list = Driver_Settings_Default
    else: 
        active_settings_list = Driver_Settings_RL[bpy.data.scenes[scene_name].render.layers.active_index]   
    driver_info = {}
    driver_modifier_info = []
    driver_modifier_info_detail = {}
    driver_variable_info = []
    driver_variable_info_detail = {}
    driver_keyframe_point_info = []
    driver_keyframe_point_info_detail = {}

    # Clean Driver Lists/Collection
    OVR_Clean_Lists_CollectionsFunction()
        
    # Clear Driver_Settings_RL for active RenderLayer
    active_settings_list.clear()
                            
    for x in DriverList_GlobalList:
      
        # Clear List (.clear() -> instance problem!)
        driver_info = {}
        driver_modifier_info = []
        driver_variable_info = []
        driver_keyframe_point_info = []
        #del driver_modifier_info[:]
        #del driver_variable_info[:]
        #del driver_keyframe_point_info[:]

        if x.driver.variables[1].name == "OVR_DriverDefault_True" and bpy.data.scenes[scene_name].render.layers.active.name != "OVR_Default_RenderLayer": # Check if User is on Default Driver RenderLayer
            driver_info["variables"] = []
            driver_info["variables"].append({})
            driver_info["variables"][0]["name"] = "OVR_DriverDefault_True" 
        else:
          
            driver_info["expression"] = x.driver.expression
            driver_info["extrapolation"] = x.extrapolation
            
            # Get Modifiers
            if x.modifiers.values() != []:
                for y in x.modifiers.values():
                  
                    # Clear List (.clear() -> instance problem!)
                    driver_modifier_info_detail = {}
                    
                    driver_modifier_info_detail["type"] = y.type
                    if y.type == "NOISE":
                        driver_modifier_info_detail["scale"] = y.scale
                        driver_modifier_info_detail["phase"] = y.phase
                        driver_modifier_info_detail["strength"] = y.strength
                        driver_modifier_info_detail["depth"] = y.depth
                        driver_modifier_info_detail["offset"] = y.offset
                    
                    elif y.type == "CYCLES":
                        driver_modifier_info_detail["mode_before"] = y.mode_before
                        driver_modifier_info_detail["mode_after"] = y.mode_after
                        driver_modifier_info_detail["cycles_before"] = y.cycles_before
                        driver_modifier_info_detail["cycles_after"] = y.cycles_after
                    
                    elif y.type == "LIMITS":
                        driver_modifier_info_detail["use_max_x"] = y.use_max_x
                        driver_modifier_info_detail["use_max_y"] = y.use_max_y
                        driver_modifier_info_detail["use_min_x"] = y.use_min_x
                        driver_modifier_info_detail["use_min_y"] = y.use_min_y
                        driver_modifier_info_detail["max_x"] = y.max_x
                        driver_modifier_info_detail["max_y"] = y.max_y
                        driver_modifier_info_detail["min_x"] = y.min_x
                        driver_modifier_info_detail["min_y"] = y.min_y
                    
                    elif y.type == "GENERATOR":
                        driver_modifier_info_detail["mode"] = y.mode
                        driver_modifier_info_detail["use_additive"] = y.use_additive
                        driver_modifier_info_detail["poly_order"] = y.poly_order
                        for z in range(y.poly_order + 1):
                            driver_modifier_info_detail["coefficients[" + str(z) + "]"] = y.coefficients[z]
                    
                    elif y.type == "STEPPED":
                        driver_modifier_info_detail["frame_step"] = y.frame_step
                        driver_modifier_info_detail["frame_offset"] = y.frame_offset
                        driver_modifier_info_detail["use_frame_start"] = y.use_frame_start
                        driver_modifier_info_detail["use_frame_end"] = y.use_frame_end
                        driver_modifier_info_detail["frame_start"] = y.frame_start
                        driver_modifier_info_detail["frame_end"] = y.frame_end
                    
                    elif y.type == "FNGENERATOR":
                        driver_modifier_info_detail["function_type"] = y.function_type
                        driver_modifier_info_detail["use_additive"] = y.use_additive
                        driver_modifier_info_detail["amplitude"] = y.amplitude
                        driver_modifier_info_detail["phase_multiplier"] = y.phase_multiplier
                        driver_modifier_info_detail["phase_offset"] = y.phase_offset
                        driver_modifier_info_detail["value_offset"] = y.value_offset
                        
                    elif y.type == "ENVELOPE":
                        driver_modifier_info_detail["reference_value"] = y.reference_value
                        driver_modifier_info_detail["default_min"] = y.default_min
                        driver_modifier_info_detail["default_max"] = y.default_max
                        driver_modifier_info_detail["control_points"] = len(y.control_points)
                        for z in range(len(y.control_points)):
                            driver_modifier_info_detail["control_points[" + str(z) + "].frame"] = y.control_points[z].frame
                            driver_modifier_info_detail["control_points[" + str(z) + "].min"] = y.control_points[z].min
                            driver_modifier_info_detail["control_points[" + str(z) + "].max"] = y.control_points[z].max
                        
                    driver_modifier_info_detail["frame_start"] = y.frame_start
                    driver_modifier_info_detail["frame_end"] = y.frame_end
                    driver_modifier_info_detail["blend_in"] = y.blend_in
                    driver_modifier_info_detail["blend_out"] = y.blend_out
                    driver_modifier_info_detail["use_influence"] = y.use_influence
                    driver_modifier_info_detail["influence"] = y.influence
                    
                    driver_modifier_info.append(driver_modifier_info_detail)
                    
                driver_info["modifiers"] = driver_modifier_info
            
            else:
                driver_info["modifiers"] = []
                         
            # Get Variables
            if len(x.driver.variables.values()) > 1:
                for y in range(1,len(x.driver.variables)):
                  
                    y = x.driver.variables[y]
                    
                    # Clear List (.clear() -> instance problem!)
                    driver_variable_info_detail = {}
                    
                    driver_variable_info_detail["name"] = y.name
                    
                    driver_variable_info_detail["type"] = y.type
                    
                    if y.type == "SINGLE_PROP":
                        driver_variable_info_detail["id_type"] = y.targets[0].id_type
                        driver_variable_info_detail["id"] = str(y.targets[0].id)
                        driver_variable_info_detail["data_path"] = y.targets[0].data_path
                    
                    if y.type == "TRANSFORMS":
                        driver_variable_info_detail["id_type"] = y.targets[0].id_type
                        driver_variable_info_detail["id"] = str(y.targets[0].id)
                        driver_variable_info_detail["transform_type"] = y.targets[0].transform_type
                        driver_variable_info_detail["transform_space"] = y.targets[0].transform_space
                    
                    if y.type == "ROTATION_DIFF":
                        driver_variable_info_detail["id_type_01"] = y.targets[0].id_type
                        driver_variable_info_detail["id_01"] = str(y.targets[0].id)
                        driver_variable_info_detail["id_type_02"] = y.targets[1].id_type
                        driver_variable_info_detail["id_02"] = str(y.targets[1].id)
                    
                    if y.type == "LOC_DIFF":
                        driver_variable_info_detail["id_type_01"] = y.targets[0].id_type
                        driver_variable_info_detail["id_01"] = str(y.targets[0].id)
                        driver_variable_info_detail["transform_space_01"] = y.targets[0].transform_space
                        driver_variable_info_detail["id_type_02"] = y.targets[1].id_type
                        driver_variable_info_detail["id_02"] = str(y.targets[1].id)
                        driver_variable_info_detail["transform_space_02"] = y.targets[1].transform_space
                    
                    driver_variable_info.append(driver_variable_info_detail)
                
                driver_info["variables"] = driver_variable_info
            
            else:    
                driver_info["variables"] = []
                
            # Get Keyframe Points
            if x.keyframe_points.values() != []:
                for y in x.keyframe_points.values():
                    
                    # Clear List (.clear() -> instance problem!)
                    driver_keyframe_point_info_detail = {}

                    driver_keyframe_point_info_detail["interpolation"] = y.interpolation      
                    driver_keyframe_point_info_detail["easing"] = y.easing       
                    driver_keyframe_point_info_detail["co.x"] = y.co.x
                    driver_keyframe_point_info_detail["co.y"] = y.co.y
                    driver_keyframe_point_info_detail["handle_left_type"] = y.handle_left_type
                    driver_keyframe_point_info_detail["handle_left.x"] = y.handle_left.x
                    driver_keyframe_point_info_detail["handle_left.y"] = y.handle_left.y
                    driver_keyframe_point_info_detail["handle_right_type"] = y.handle_right_type
                    driver_keyframe_point_info_detail["handle_right.x"] = y.handle_right.x
                    driver_keyframe_point_info_detail["handle_right.y"] = y.handle_right.y

                    driver_keyframe_point_info.append(driver_keyframe_point_info_detail)
                    
                driver_info["keyframe_points"] = driver_keyframe_point_info
            
            else:
                driver_info["keyframe_points"] = []
                
        # Add Driver Settings to RenderLayer List with Driver Count Var as Key       
        active_settings_list[x.driver.variables[0].name] = driver_info
    
        # Reset Driver To Defaults
        if x.modifiers.values() != []:
            for y in x.modifiers.values():
              x.modifiers.remove(y)
        if len(x.driver.variables.values()) > 1:
            for y in range(1,len(x.driver.variables)):
                y = x.driver.variables[y]
                x.driver.variables.remove(y)
        while x.keyframe_points.values() != []:
            y = x.keyframe_points[0]
            x.keyframe_points.remove(y)
                
    # print("OVR | Saved Drivers for Render Layer '" + bpy.data.scenes[scene_name].render.layers.active.name + "'")
                     
def OVR_PasteDriversSettingsFunction():
  
    # Globals
    global DriverList_GlobalList
    global Driver_Settings_RL
    global Driver_Settings_Default
    
    # Variables
    scene_name = bpy.context.scene.name      
    if bpy.data.scenes[scene_name].render.layers.active.name == "OVR_Default_RenderLayer": # Check if User is on Default Driver RenderLayer
        active_settings_list = Driver_Settings_Default
    else: 
        active_settings_list = Driver_Settings_RL[bpy.data.scenes[scene_name].render.layers.active_index]   
    driver_info = {}
    
    for x in DriverList_GlobalList:
      
        # Clear List (.clear() -> instance problem!)
        driver_info = {}
        
        driver_info = active_settings_list[x.driver.variables[0].name]
        
        if driver_info["variables"][0]["name"] == "OVR_DriverDefault_True" and bpy.data.scenes[scene_name].render.layers.active.name != "OVR_Default_RenderLayer": # Check if User is on Default Driver RenderLayer 
            driver_info = Driver_Settings_Default[x.driver.variables[0].name]           
            
        x.driver.expression = str(driver_info["expression"])  # Convert to String > So that None content doesn't create "not a string" error ... for some reason
        x.extrapolation = driver_info["extrapolation"]
        
        # Set Modifiers
        if driver_info["modifiers"] != []:       
            for y in driver_info["modifiers"]: 
                modifier = x.modifiers.new(y["type"])
                
                if y["type"] == "NOISE":
                    modifier.scale = y["scale"] 
                    modifier.phase = y["phase"] 
                    modifier.strength = y["strength"] 
                    modifier.depth = y["depth"] 
                    modifier.offset = y["offset"] 
                
                elif y["type"] == "CYCLES":
                    modifier.mode_before = y["mode_before"] 
                    modifier.mode_after = y["mode_after"] 
                    modifier.cycles_before = y["cycles_before"] 
                    modifier.cycles_after = y["cycles_after"] 
                
                elif y["type"] == "LIMITS":
                    modifier.use_max_x = y["use_max_x"] 
                    modifier.use_max_y = y["use_max_y"] 
                    modifier.use_min_x = y["use_min_x"] 
                    modifier.use_min_y = y["use_min_y"] 
                    modifier.max_x = y["max_x"] 
                    modifier.max_y = y["max_y"] 
                    modifier.min_x = y["min_x"] 
                    modifier.min_y = y["min_y"] 
                
                elif y["type"] == "GENERATOR":
                    modifier.mode = y["mode"] 
                    modifier.use_additive = y["use_additive"] 
                    modifier.poly_order = y["poly_order"] 
                    for z in range(y["poly_order"] + 1):
                        modifier.coefficients[z] =  y["coefficients[" + str(z) + "]"] 
                
                elif y["type"] == "STEPPED":
                    modifier.frame_step = y["frame_step"] 
                    modifier.frame_offset = y["frame_offset"] 
                    modifier.use_frame_start = y["use_frame_start"] 
                    modifier.use_frame_end = y["use_frame_end"] 
                    modifier.frame_start = y["frame_start"] 
                    modifier.frame_end = y["frame_end"] 
                
                elif y["type"] == "FNGENERATOR":
                    modifier.function_type = y["function_type"] 
                    modifier.use_additive = y["use_additive"] 
                    modifier.amplitude = y["amplitude"] 
                    modifier.phase_multiplier = y["phase_multiplier"] 
                    modifier.phase_offset = y["phase_offset"] 
                    modifier.value_offset = y["value_offset"] 
                
                elif y["type"] == "ENVELOPE":
                        modifier.reference_value  = y["reference_value"] 
                        modifier.default_min  = y["default_min"] 
                        modifier.default_max  = y["default_max"] 
                        for z in range(y["control_points"]):
                            modifier.control_points.add(y["control_points[" + str(z) + "].frame"])
                            modifier.control_points[z].min = y["control_points[" + str(z) + "].min"] 
                            modifier.control_points[z].max  = y["control_points[" + str(z) + "].max"] 

                modifier.frame_start = y["frame_start"] 
                modifier.frame_end = y["frame_end"] 
                modifier.blend_in = y["blend_in"] 
                modifier.blend_out = y["blend_out"] 
                modifier.use_influence = y["use_influence"] 
                modifier.influence = y["influence"]   
                
        # Set Variables
        if driver_info["variables"] != []:       
            for y in driver_info["variables"]: 
                variable = x.driver.variables.new()
                
                variable.name = y["name"]
                
                variable.type = y["type"]
                
                if y["type"] == "SINGLE_PROP":
                    variable.targets[0].id_type = y["id_type"]
                    variable.targets[0].id = eval(y["id"])
                    variable.targets[0].data_path = y["data_path"]
                
                if y["type"] == "TRANSFORMS":
                    variable.targets[0].id_type = y["id_type"]
                    variable.targets[0].id = eval(y["id"])
                    variable.targets[0].transform_type = y["transform_type"]
                    variable.targets[0].transform_space = y["transform_space"]
                
                if y["type"] == "ROTATION_DIFF":
                    variable.targets[0].id_type_01 = y["id_type_01"]
                    variable.targets[0].id_01 = eval(y["id_01"])
                    variable.targets[1].id_type_02 = y["id_type_02"]
                    variable.targets[1].id_02 = eval(y["id_02"])
                
                if y["type"] == "LOC_DIFF":
                    variable.targets[0].id_type_01 = y["id_type_01"]
                    variable.targets[0].id_01 = eval(y["id_01"])
                    variable.targets[0].transform_space_01 = y["transform_space_01"]
                    variable.targets[1].id_type_02 = y["id_type_02"]
                    variable.targets[1].id_02 = eval(y["id_02"])
                    variable.targets[1].transform_space_02 = y["transform_space_02"]                  
          
        # Set Keyframe Points
        if driver_info["keyframe_points"] != []:       
            for y in driver_info["keyframe_points"]: 
                keyframe_points = x.keyframe_points.insert(y["co.x"],y["co.y"])
                keyframe_points.handle_left_type = y["handle_left_type"]
                keyframe_points.handle_left.x = y["handle_left.x"]
                keyframe_points.handle_left.y = y["handle_left.y"]
                keyframe_points.handle_right_type = y["handle_right_type"]
                keyframe_points.handle_right.x = y["handle_right.x"]
                keyframe_points.handle_right.y = y["handle_right.y"]
                keyframe_points.interpolation = y["interpolation"]
                keyframe_points.easing = y["easing"]   
                
    # print("OVR | Pasted Drivers for Render Layer '" + bpy.data.scenes[scene_name].render.layers.active.name + "'")

def OVR_AddDriversSettingsFunction():
  
    # Globals
    global DriverList_GlobalList
    global Driver_Settings_RL
    
    # Variables
    scene_name = bpy.context.scene.name  
    
    # Create new Driver_Settings_RL for newly created layer
    Driver_Settings_RL.append({})
    active_settings_list = Driver_Settings_RL[bpy.data.scenes[scene_name].render.layers.active_index]   
    
    for x in DriverList_GlobalList:
        driver_info = {}
        driver_info["variables"] = []
        driver_info["variables"].append({})
        driver_info["variables"][0]["name"] = "OVR_DriverDefault_True" 
        active_settings_list[x.driver.variables[0].name] = driver_info
        
def OVR_DeleteDriversSettings():
  
    # Globals
    global DriverList_GlobalList
    global Driver_Settings_RL
    
    # Variables
    scene_name = bpy.context.scene.name  
    
    del Driver_Settings_RL[bpy.data.scenes[scene_name].render.layers.active_index] 
    
    # Reset Driver To Defaults
    for x in DriverList_GlobalList:
        if x.modifiers.values() != []:
            for y in x.modifiers.values():
              x.modifiers.remove(y)
        if len(x.driver.variables.values()) > 1:
            for y in range(1,len(x.driver.variables)):
                y = x.driver.variables[y]
                x.driver.variables.remove(y)
        while x.keyframe_points.values() != []:
            y = x.keyframe_points[0]
            x.keyframe_points.remove(y)
    
# Active Render Layer Callback    
def OVR_Property_ActiveRenderLayer_Callback(self, context):
    
    # Globals
    global addORremove
    # Variables
    scene_name = bpy.context.scene.name
    
    if addORremove == True:
        pass
    else:
        OVR_CopyDriversSettingsFunction()
        bpy.data.scenes[scene_name].render.layers.active_index = bpy.data.scenes[scene_name].OVR_ActiveRenderLayerIndex
        OVR_PasteDriversSettingsFunction()
    
    # Refresh Scene
    bpy.context.scene.update()
    bpy.context.scene.frame_set(bpy.context.scene.frame_current)

    return None
        
# UI Creation | RenderLayers
class OVR_RenderLayer_OVERRIDE(Panel):
    bl_idname = "ovr.override_render_layers"
    bl_label = "OVERRIDE Render Layers"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render_layer"

    def draw(self, context):
        scene_name = bpy.context.scene.name
        layout = self.layout

        scene = bpy.data.scenes[scene_name]
        rd = scene.render

        row = layout.row()
        col = row.column()
        col.template_list("RENDERLAYER_UL_renderlayers", "", rd, "layers", scene, "OVR_ActiveRenderLayerIndex")

        col = row.column()
        sub = col.column(align=True)
        sub.operator("ovr.addrenderlayer", icon='ZOOMIN', text="")
        sub.operator("ovr.removerenderlayer", icon='ZOOMOUT', text="")
        col.prop(rd, "use_single_layer", icon_only=True)

# Render Layer Add & Remove  
class OVR_AddRenderLayer(bpy.types.Operator):
    bl_idname = "ovr.addrenderlayer"
    bl_label = "Add Render Layer"
    bl_description = "Add a render layer"
    bl_options = {"REGISTER"}
   
    def execute(self, context) :
      
        # Globals
        global addORremove
        # Variables
        scene_name = bpy.context.scene.name
        
        addORremove = True
        
        # Remove OVR_Default_RenderLayer
        temporary_index = bpy.data.scenes[scene_name].render.layers.active_index
        bpy.data.scenes[scene_name].render.layers.remove(bpy.data.scenes[scene_name].render.layers["OVR_Default_RenderLayer"])
        bpy.data.scenes[scene_name].render.layers.active_index = temporary_index
        
        OVR_CopyDriversSettingsFunction()
        bpy.ops.scene.render_layer_add()
        bpy.data.scenes[scene_name].OVR_ActiveRenderLayerIndex = bpy.data.scenes[scene_name].render.layers.active_index
        OVR_AddDriversSettingsFunction()  
        OVR_PasteDriversSettingsFunction() 
        
        # Re-Add OVR_Default_RenderLayer
        OVR_Default_RenderLayer = bpy.data.scenes[scene_name].render.layers.new("OVR_Default_RenderLayer") 
        OVR_Default_RenderLayer_Settings_Function(OVR_Default_RenderLayer)
    
        addORremove = False
        
        # print ("OVR | Render Layer Added")
        
        return {"FINISHED"}
      
class OVR_RemoveRenderLayer(bpy.types.Operator):
    bl_idname = "ovr.removerenderlayer"
    bl_label = "Remove Render Layer"
    bl_description = "Remove the selected render layer"
    bl_options = {"REGISTER"}
   
    def execute(self, context) :
        # Globals
        global addORremove
        # Variables
        scene_name = bpy.context.scene.name
        
        if bpy.data.scenes[scene_name].render.layers.active.name == "OVR_Default_RenderLayer":
            self.report({'WARNING'}, "OVR | Can't Delete This Layer")
            return {"CANCELLED"}
          
        if len(bpy.data.scenes[scene_name].render.layers.values()) > 2:
            addORremove = True
            OVR_Clean_Lists_CollectionsFunction()
            OVR_DeleteDriversSettings()
            bpy.ops.scene.render_layer_remove()
            bpy.data.scenes[scene_name].OVR_ActiveRenderLayerIndex = 0
            OVR_PasteDriversSettingsFunction() 
            
            addORremove = False
            
            # print ("OVR | Render Layer Removed")
            
        return {"FINISHED"}
  
############################################################################################################### Render Layer Functions

############################################################################################################### Render Functions

def OVR_Render_Create_Default_File_Structure_Function(RenderLayerName):

    # Variables
    scene_name = bpy.context.scene.name 
    scene_render = bpy.data.scenes[scene_name].render
    render_file_path = bpy.data.scenes[scene_name].render.filepath
    Image_000_Creation = {}
    Image_000_Creation["render_enable_layer_list"] = []
    Image_000_Creation["render_render_obj_list"] = []

    # Safe Render Settings
    for x in scene_render.layers:
        if x.use == True:
            x.use = False
            Image_000_Creation["render_enable_layer_list"].append(x)
    for x in bpy.data.objects:
        if x.hide_render == False:
            x.hide_render = True
            Image_000_Creation["render_render_obj_list"].append(x)
    Image_000_Creation["resolution_x"] = scene_render.resolution_x
    Image_000_Creation["resolution_y"] = scene_render.resolution_y
    Image_000_Creation["resolution_percentage"] = scene_render.resolution_percentage
    Image_000_Creation["filepath"] = scene_render.filepath
    Image_000_Creation["use_sequencer"] = scene_render.use_sequencer
    Image_000_Creation["use_compositing"] = scene_render.use_compositing
    
    # Change Render Settings
    scene_render.resolution_x = 4
    scene_render.resolution_y = 4
    scene_render.resolution_percentage = 100
    scene_render.image_settings.file_format = 'OPEN_EXR_MULTILAYER'
    scene_render.image_settings.color_mode = 'RGBA'
    scene_render.use_sequencer = False
    scene_render.use_compositing = False
    
    # Render
    active_rl_safe = bpy.data.scenes[scene_name].render.layers.active
    active_rl = bpy.data.scenes[scene_name].render.layers[RenderLayerName]
    bpy.data.scenes[scene_name].render.layers.active = active_rl
    active_rl_index = bpy.data.scenes[scene_name].render.layers.active_index
    active_rl.use = True
    bpy.data.scenes[scene_name].render.filepath = render_file_path + str(active_rl_index) + "_" + active_rl.name + "\\" + active_rl.name + "_0001"
    bpy.ops.render.render(write_still=True)
    bpy.data.scenes[scene_name].render.layers.active = active_rl_safe
    active_rl.use = False
    
    # Revert Render Settings
    scene_render.resolution_x = Image_000_Creation["resolution_x"] 
    scene_render.resolution_y = Image_000_Creation["resolution_y"]
    scene_render.resolution_percentage = Image_000_Creation["resolution_percentage"]
    scene_render.filepath = Image_000_Creation["filepath"] 
    scene_render.use_sequencer = Image_000_Creation["use_sequencer"]
    scene_render.use_compositing = Image_000_Creation["use_compositing"]

    for x in Image_000_Creation["render_enable_layer_list"]:
        x.use = True
    for x in Image_000_Creation["render_render_obj_list"]:
        x.hide_render = False

def OVR_Render_Swap_Node_Function(Node):
    
    # Variables
    scene_name = bpy.context.scene.name 
    render_file_path = bpy.data.scenes[scene_name].render.filepath
    NodeInfo = {}
    Combined_Exists = False
    Output_Name = ""
    Reroute_Location_X = 50
    Reroute_Location_Offset = 1
    
    # Get Node Info
    NodeInfo['location'] = eval(str(Node.location)[8:-1])
    NodeInfo['width'] = Node.width
    NodeInfo['hide'] = Node.hide
    NodeInfo['Render_Layer'] = Node.layer
    NodeInfo['outputs'] = []  
    for x in Node.outputs:
        if x.enabled == True:
            # Fix Name For Alphabetical Order
            if x.name == "Image":
                Output_Name = "Combined"
                Combined_Exists = True
                Reroute_Location_X = 125
            elif x.name == "Z": 
                Output_Name = "Depth"
            elif x.name == "Diffuse Color": 
                Output_Name = "DiffCol"
            elif x.name == "Diffuse Direct": 
                Output_Name = "DiffDir"
            elif x.name == "Diffuse Indirect": 
                Output_Name = "DiffInd"
            elif x.name == "Glossy Color": 
                Output_Name = "GlossCol"
            elif x.name == "Glossy Direct": 
                Output_Name = "GlossDir"
            elif x.name == "Glossy Indirect": 
                Output_Name = "GlossInd"
            elif x.name == "Transmission Color": 
                Output_Name = "TransCol"
            elif x.name == "Transmission Direct": 
                Output_Name = "TransDir"
            elif x.name == "Transmission Indirect": 
                Output_Name = "TransInd"
            elif x.name == "Subsurface Color": 
                Output_Name = "SubsurfaceCol"
            elif x.name == "Subsurface Direct": 
                Output_Name = "SubsurfaceDir"
            elif x.name == "Subsurface Indirect": 
                Output_Name = "SubsurfaceInd"
            elif x.name == "Environment": 
                Output_Name = "Env"
            elif x.name == "Z": 
                Output_Name = "Depth"
            elif x.name == "Speed":
                Output_Name = "Vector"
            else:
                Output_Name = x.name
            OutputInfo = {}
            OutputInfo['name'] = Output_Name 
            OutputInfo['to_socket'] = []
            NodeInfo_Output_Links_Socket = []
            for y in x.links:
                NodeInfo_Output_Links_Socket.append(y.to_socket)    
            OutputInfo['to_socket'] = NodeInfo_Output_Links_Socket
            NodeInfo['outputs'].append(OutputInfo)
            
    NodeInfo['outputs'] = sorted(NodeInfo['outputs'], key=lambda k: k['name']) # Sort Alphabetically
    
    # Swap Render Layer Node to Image Node
    bpy.data.scenes[scene_name].node_tree.nodes.remove(Node)
    ImageNode = bpy.data.scenes[scene_name].node_tree.nodes.new('CompositorNodeImage')
    ImageNode.label = NodeInfo['Render_Layer']
    ImageNode.location = NodeInfo['location']
    ImageNode.width = NodeInfo['width']
    ImageNode.hide = NodeInfo['hide'] 
    ImageNode.show_preview = False
    ImageNode.use_auto_refresh = True
    ImageNode.frame_start = bpy.data.scenes[scene_name].frame_start
    ImageNode.frame_duration = bpy.data.scenes[scene_name].frame_end
    
    # Load Image Seq Data Block
    for x in range(len(bpy.data.scenes[scene_name].render.layers.values())-1):
        if bpy.data.scenes[scene_name].render.layers[x].name == NodeInfo['Render_Layer']:
            Render_Layer_Index = x
            break
    if NodeInfo['Render_Layer']+"_0001.exr" not in bpy.data.images:
        Placeholder_Image = bpy.data.images.load(render_file_path + str(Render_Layer_Index) + "_" + NodeInfo['Render_Layer'] + "\\" + NodeInfo['Render_Layer'] + "_0001.exr")
        Placeholder_Image.source = 'SEQUENCE'
    else:
        Placeholder_Image = bpy.data.images[NodeInfo['Render_Layer'] + "_0001.exr"]
        Placeholder_Image.reload()
    ImageNode.image = bpy.data.images[NodeInfo['Render_Layer']+"_0001.exr"]
    
    # Create Reroutes    
    for x in range(len(NodeInfo['outputs'])):
        RerouteNode = bpy.data.scenes[scene_name].node_tree.nodes.new('NodeReroute')
        RerouteNode.name = "Reroute_" + NodeInfo['Render_Layer'] + "_" + NodeInfo['outputs'][x]['name'] 
        RerouteNode.label = "Reroute_" + NodeInfo['Render_Layer'] + "_" + NodeInfo['outputs'][x]['name'] 
        if NodeInfo['outputs'][x]['name'] == "Combined":
            RerouteNode.location = (NodeInfo['location'][0] + NodeInfo['width'] + Reroute_Location_X,NodeInfo['location'][1] -15)
        elif NodeInfo['outputs'][x]['name'] == "Alpha":
            SeparateRGBANode = bpy.data.scenes[scene_name].node_tree.nodes.new('CompositorNodeSepRGBA')
            SeparateRGBANode.name = "SeparateRGBA_" + NodeInfo['Render_Layer'] + "_Alpha"
            SeparateRGBANode.label = "SeparateRGBA_" + NodeInfo['Render_Layer'] + "_Alpha" 
            SeparateRGBANode.location = (NodeInfo['location'][0] + NodeInfo['width'] + 20,NodeInfo['location'][1] - 25)
            SeparateRGBANode.hide = True
            SeparateRGBANode.outputs['R'].hide = True 
            SeparateRGBANode.outputs['G'].hide = True 
            SeparateRGBANode.outputs['B'].hide = True 
            bpy.data.scenes[scene_name].node_tree.links.new(SeparateRGBANode.outputs['A'],RerouteNode.inputs['Input'])
            RerouteNode.location = (NodeInfo['location'][0] + NodeInfo['width'] + Reroute_Location_X,NodeInfo['location'][1] -15 - 22)
        else:  
            RerouteNode.location = (NodeInfo['location'][0] + NodeInfo['width'] + Reroute_Location_X,NodeInfo['location'][1] -36 - (21.80*Reroute_Location_Offset))
            Reroute_Location_Offset += 1
        for y in NodeInfo['outputs'][x]['to_socket']:
            bpy.data.scenes[scene_name].node_tree.links.new(RerouteNode.outputs['Output'],y)
    
    # Create Links to Reroutes  
    for x in ImageNode.outputs:
        if x.name == "Combined":
            if Combined_Exists == True:
                bpy.data.scenes[scene_name].node_tree.links.new(x,bpy.data.scenes[scene_name].node_tree.nodes["Reroute_"+ImageNode.label+"_"+x.name].inputs['Input'])
                bpy.data.scenes[scene_name].node_tree.links.new(x,bpy.data.scenes[scene_name].node_tree.nodes["SeparateRGBA_"+ImageNode.label+"_Alpha"].inputs['Image'])
        else:
            bpy.data.scenes[scene_name].node_tree.links.new(x,bpy.data.scenes[scene_name].node_tree.nodes["Reroute_"+ImageNode.label+"_"+x.name].inputs['Input'])

def OVR_Render_Image_Function(self,Start_Frame,End_Frame,Frame_Step):

    # Variables
    scene_name = bpy.context.scene.name 
    Compositor_Node_Tree = bpy.data.scenes[scene_name].node_tree 
    Render_Sleep_Time = bpy.data.scenes[bpy.context.scene.name].OVR_DriverSettings.Activate_Abort_Render_Timer_Time
    Render_File_Path_Base = bpy.data.scenes[scene_name].render.filepath
    Render_Start_Frame = Start_Frame
    Render_End_Frame = End_Frame
    Render_Frame_Step = Frame_Step
    Render_Abort = False
    Render_RenderLayer_Use = []
    Render_RenderLayer_Use_Index = {}
    Render_Result_Image_Data_Block_Name = None
    for x in bpy.data.images:
        if x.type == "RENDER_RESULT":
            Render_Result_Image_Data_Block_Name = x.name
            break
    if Render_Result_Image_Data_Block_Name == None:
        Render_Result_Image_Data_Block_Name = "Render Result"
    active_rl_safe = bpy.data.scenes[scene_name].render.layers.active
    active_rl_index_safe = bpy.data.scenes[scene_name].render.layers.active_index
    composite_state_safe = bpy.data.scenes[scene_name].render.use_compositing
    sequencer_state_safe = bpy.data.scenes[scene_name].render.use_sequencer
    bpy.data.scenes[scene_name].render.use_compositing = False
    bpy.data.scenes[scene_name].render.use_sequencer = False 

    # Check if Resolution is ok / Get Resolution Percentage for "OVR_Default_RenderLayer Composite" Render
    res_x = bpy.data.scenes[scene_name].render.resolution_x * bpy.data.scenes[scene_name].render.border_max_x
    res_y = bpy.data.scenes[scene_name].render.resolution_y * bpy.data.scenes[scene_name].render.border_max_y
    res_percentage = bpy.data.scenes[scene_name].render.resolution_percentage * 0.01
    if (res_x * res_percentage) < 1 or (res_y * res_percentage) < 1:
        print("OVR | ------------- Image Resolution Too Small -------------")
        self.report({'WARNING'}, "OVR | Image Resolution Too Small")
        return {"CANCELLED"} 

    # Get Used RenderLayers / Set Settings
    if bpy.data.scenes[scene_name].render.use_single_layer == True:
        if bpy.data.scenes[scene_name].render.layers.active.name != "OVR_Default_RenderLayer":
            Render_RenderLayer_Use.append(bpy.data.scenes[scene_name].render.layers.active)
            Render_RenderLayer_Use_Index[bpy.data.scenes[scene_name].render.layers.active.name] = bpy.data.scenes[scene_name].render.layers.active_index
        else:
            print("OVR | ------------- Can't Render 'OVR_Default_RenderLayer' -------------")
            self.report({'WARNING'}, "OVR | Can't Render 'OVR_Default_RenderLayer'")
            return {"CANCELLED"}               
    else:
        for x in bpy.data.scenes[scene_name].render.layers:
            if x.name != "OVR_Default_RenderLayer":
                if x.use == True:
                    bpy.data.scenes[scene_name].render.layers.active = x
                    Render_RenderLayer_Use.append(x)
                    Render_RenderLayer_Use_Index[x.name] = bpy.data.scenes[scene_name].render.layers.active_index
        bpy.data.scenes[scene_name].render.layers.active = active_rl_safe
    if Render_RenderLayer_Use == []:
        print("OVR | ------------- All Render Layers Are Disabled -------------")
        self.report({'WARNING'}, "OVR | All Render Layers Are Disabled")
        return {"CANCELLED"} 

    # Show Image Editor
    bpy.ops.render.view_show('INVOKE_DEFAULT') 

    bpy.ops.wm.console_toggle()
        
    # Cycles Doesn't Trigger Exception
    for x in range(Render_Start_Frame, Render_End_Frame, Render_Frame_Step):
        # Set File_Path
        if bpy.data.scenes[scene_name].render.filepath[-1:] == "\\":
            bpy.data.scenes[scene_name].render.filepath = bpy.path.abspath(Render_File_Path_Base) + "\\"   
        else:
            bpy.data.scenes[scene_name].render.filepath = bpy.path.abspath(Render_File_Path_Base)
        file_ext = bpy.data.scenes[scene_name].render.file_extension

        bpy.data.scenes[scene_name].frame_set(x)
        Current_Frame = str(bpy.data.scenes[scene_name].frame_current)               
        if len(Current_Frame) == 1:
            Current_Frame = "000" + Current_Frame
        if len(Current_Frame) == 2:
            Current_Frame = "00" + Current_Frame
        if len(Current_Frame) == 3:
            Current_Frame = "0" + Current_Frame  

        temp_file_path_end = bpy.data.scenes[scene_name].render.filepath[bpy.data.scenes[scene_name].render.filepath.rfind("\\")+1:]    
        if bpy.data.scenes[scene_name].render.use_file_extension == True:
            if bpy.data.scenes[scene_name].render.filepath[-1:] != "_" and temp_file_path_end != '':
                bpy.data.scenes[scene_name].render.filepath = bpy.data.scenes[scene_name].render.filepath + "_"
            bpy.data.scenes[scene_name].render.filepath = bpy.data.scenes[scene_name].render.filepath + Current_Frame + file_ext
        else:
            temp_own_file_ext = None
            if temp_file_path_end != '':
                if temp_file_path_end.rfind(".") != -1:
                    temp_own_file_ext = bpy.data.scenes[scene_name].render.filepath[bpy.data.scenes[scene_name].render.filepath.rfind("."):]
                    bpy.data.scenes[scene_name].render.filepath = bpy.data.scenes[scene_name].render.filepath[:bpy.data.scenes[scene_name].render.filepath.rfind(".")]
                if bpy.data.scenes[scene_name].render.filepath[-1:] != "_":
                    bpy.data.scenes[scene_name].render.filepath = bpy.data.scenes[scene_name].render.filepath + "_"
                if temp_own_file_ext != None:
                    bpy.data.scenes[scene_name].render.filepath = bpy.data.scenes[scene_name].render.filepath + Current_Frame + temp_own_file_ext
                else:
                    bpy.data.scenes[scene_name].render.filepath = bpy.data.scenes[scene_name].render.filepath + Current_Frame
            else:
                bpy.data.scenes[scene_name].render.filepath = bpy.data.scenes[scene_name].render.filepath + Current_Frame
        while os.path.exists(bpy.data.scenes[scene_name].render.filepath) == True and bpy.data.scenes[scene_name].render.use_overwrite == False:
            bpy.data.scenes[scene_name].render.filepath = bpy.data.scenes[scene_name].render.filepath.replace("_"+Current_Frame,".001_"+Current_Frame)
     
        # Check for Render Abort         
        try:
            print("OVR | Press Ctrl+C Now in CMD Window to Abort the Rendering Process")
            time.sleep(Render_Sleep_Time)
            print("OVR | Started Rendering Frame "+str(bpy.data.scenes[scene_name].frame_current))
        except KeyboardInterrupt:
            Render_Abort = True
          
        # Render
        Render_Time_Start = time.time()
        Render_RenderLayer_Counter = 0
        for y in Render_RenderLayer_Use:   
            if Render_Abort == False: 
                Render_RenderLayer_Counter += 1
                bpy.data.scenes[scene_name].OVR_ActiveRenderLayerIndex = Render_RenderLayer_Use_Index[y.name]
                print("OVR | Rendering Frame "+str(bpy.data.scenes[scene_name].frame_current)+" Layer '"+y.name+"'")
                if Render_RenderLayer_Counter != len(Render_RenderLayer_Use):
                    bpy.ops.render.render(layer=y.name)   
                else: 
                    if composite_state_safe == True: # Render Composite Last
                        if bpy.data.scenes[scene_name].use_nodes == True:
                            bpy.ops.render.render(layer=y.name) 
                            print("OVR | Rendering Frame "+str(bpy.data.scenes[scene_name].frame_current)+" Composite")
                            global NAP_Frame_Handler_Skip   
                            border_min_x_safe = bpy.data.scenes[scene_name].render.border_min_x
                            border_min_y_safe = bpy.data.scenes[scene_name].render.border_min_y  
                            border_max_x_safe = bpy.data.scenes[scene_name].render.border_max_x
                            border_max_y_safe = bpy.data.scenes[scene_name].render.border_max_y    
                            tile_x_safe = bpy.data.scenes[scene_name].render.tile_x
                            tile_y_safe = bpy.data.scenes[scene_name].render.tile_y    
                            bpy.data.scenes[scene_name].render.border_min_x = 0
                            bpy.data.scenes[scene_name].render.border_min_y = 0       
                            bpy.data.scenes[scene_name].render.border_max_x = 1
                            bpy.data.scenes[scene_name].render.border_max_y = 1
                            bpy.data.scenes[scene_name].render.tile_x = 512
                            bpy.data.scenes[scene_name].render.tile_y = 512
                            NAP_Frame_Handler_Skip = True
                            bpy.data.scenes[scene_name].render.use_compositing = True
                            bpy.data.scenes[scene_name].render.use_sequencer = True 
                            bpy.ops.render.render(layer="OVR_Default_RenderLayer",write_still=True)
                            bpy.data.scenes[scene_name].render.use_compositing = False # Unnecessary
                            bpy.data.scenes[scene_name].render.use_sequencer = False # Unnecessary
                            NAP_Frame_Handler_Skip = False
                            bpy.data.scenes[scene_name].render.tile_x = tile_x_safe
                            bpy.data.scenes[scene_name].render.tile_y = tile_y_safe
                            bpy.data.scenes[scene_name].render.border_min_x = border_min_x_safe
                            bpy.data.scenes[scene_name].render.border_min_y = border_min_y_safe
                            bpy.data.scenes[scene_name].render.border_max_x = border_max_x_safe
                            bpy.data.scenes[scene_name].render.border_max_y = border_max_y_safe
                        else:
                            bpy.ops.render.render(layer=y.name,write_still=True) 
                    else:
                        bpy.ops.render.render(layer=y.name,write_still=True) 
            else:
                bpy.data.scenes[scene_name].OVR_ActiveRenderLayerIndex = active_rl_index_safe
                bpy.data.scenes[scene_name].render.filepath = Render_File_Path_Base
                bpy.data.scenes[scene_name].render.use_compositing = composite_state_safe 
                bpy.data.scenes[scene_name].render.use_sequencer = sequencer_state_safe
                bpy.ops.wm.console_toggle()
                print("OVR | ------------- Rendering Cancelled -------------")
                return {"CANCELLED"} 

        # Print Render Finished / Time
        Render_Time_Elapsed = (round(time.time()-Render_Time_Start,2)) 
        m, s = divmod(Render_Time_Elapsed, 60)
        h, m = divmod(m, 60)
        Render_Time_Elapsed_String = "%d:%02d:%02g" % (h, m, s)
        print("OVR | Finished Rendering Frame "+str(bpy.data.scenes[scene_name].frame_current)+" > Time "+Render_Time_Elapsed_String)  
        self.report({'INFO'}, "OVR | Finished Rendering Frame "+str(bpy.data.scenes[scene_name].frame_current)+" > Time "+Render_Time_Elapsed_String)           
    
    # Revert Settings
    bpy.data.scenes[scene_name].OVR_ActiveRenderLayerIndex = active_rl_index_safe
    bpy.data.scenes[scene_name].render.filepath = Render_File_Path_Base
    bpy.data.scenes[scene_name].render.use_compositing = composite_state_safe 
    bpy.data.scenes[scene_name].render.use_sequencer = sequencer_state_safe    
    bpy.ops.wm.console_toggle()    

    print("OVR | ------------- Rendering Finished -------------")
     
    return {"FINISHED"}  

# UI Creation | Render Menu
class OVR_RenderLayer_OVERRIDE_Render_Menu(Panel):
    bl_idname = "ovr.overide_render_menu"
    bl_label = "OVERRIDE Render Menu"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render_layer"

    def draw(self, context):
        scene_name = bpy.context.scene.name 
        layout = self.layout

        rd = context.scene.render

        row = layout.row(align=True)
        row.operator("ovr.render_image", text="Render", icon='RENDER_STILL')
        row.operator("ovr.render_animation", text="Animation", icon='RENDER_ANIMATION')
        
        ''' Swap Nodes
        row = layout.row(align=False)
        row.operator("ovr.generate_compositor_setup", text="Generate Compositor Setup", icon='NODETREE')
        '''
        
        row = layout.row(align=False)
        row.prop(bpy.data.scenes[scene_name].OVR_DriverSettings, "Override_Render_Hotkeys")
        row.prop(bpy.data.scenes[scene_name].OVR_DriverSettings, "Activate_Abort_Render_Timer_Time")
        
class OVR_Generate_Compositor_Setup(bpy.types.Operator):
    bl_idname = "ovr.generate_compositor_setup"
    bl_label = "Generate Compositor Setup"
    bl_description = "Swap RenderLayer Nodes to Image Nodes and Reload Image Data Blocks"
    bl_options = {"REGISTER","UNDO"}

    def execute(self, context):
        
        # Variables
        scene_name = bpy.context.scene.name 
        active_node = ""
        selected_nodes = []
        current_frame_safe = bpy.data.scenes[scene_name].frame_current  

        if bpy.data.scenes[scene_name].use_nodes == True:
            
            bpy.data.scenes[scene_name].frame_set(0)
            Compositor_Node_Tree = bpy.data.scenes[scene_name].node_tree 
            
            # Safe Node Selection
            if bpy.data.scenes[scene_name].node_tree.nodes.active != "":
                active_node = bpy.data.scenes[scene_name].node_tree.nodes.active 
            for x in Compositor_Node_Tree.nodes:
                if x.select == True:
                    selected_nodes.append(x)    
            
            # Execute Main Script
            for x in Compositor_Node_Tree.nodes:
                if x.type == 'R_LAYERS' and x.layer != "OVR_Default_RenderLayer":
                    OVR_Render_Create_Default_File_Structure_Function(x.layer)
                    OVR_Render_Swap_Node_Function(x)
                elif x.type == 'IMAGE':
                    x.image.reload()
                
            # Restore Node Selection
            for x in Compositor_Node_Tree.nodes:
                x.select = False
            if active_node != "":
                bpy.data.scenes[scene_name].node_tree.nodes.active = active_node
            for x in selected_nodes:
                x.select = True

            current_frame_safe = bpy.data.scenes[scene_name].frame_current
            
        print ("OVR | Compositor Setup Generated")
        
        return {"FINISHED"}    
    
class OVR_Render_Image(bpy.types.Operator):
    bl_idname = "ovr.render_image"
    bl_label = "Render Current Frame"
    bl_description = "Render Current Frame"
    bl_options = {"REGISTER"}

    def execute(self, context):
        scene_name = bpy.context.scene.name 
        print("OVR | ------------- Rendering Image -------------")
        return OVR_Render_Image_Function(self,bpy.data.scenes[scene_name].frame_current,bpy.data.scenes[scene_name].frame_current+1,1)       
    
class OVR_Render_Animation(bpy.types.Operator):
    bl_idname = "ovr.render_animation"
    bl_label = "Render Animation"
    bl_description = "Render Animation"
    bl_options = {"REGISTER"}

    def execute(self, context):
        scene_name = bpy.context.scene.name 
        print("OVR | ------------- Rendering Animation -------------")
        return OVR_Render_Image_Function(self,bpy.data.scenes[scene_name].frame_start,bpy.data.scenes[scene_name].frame_end+1,bpy.data.scenes[scene_name].frame_step)

def OVR_Override_Render_Hotkeys_Add_Function():
    km = bpy.context.window_manager.keyconfigs.addon.keymaps.new(name='Screen', space_type='EMPTY')
    kmi = km.keymap_items.new('ovr.render_image','F12','PRESS')
    Override_Render_Hotkeys_keymaps.append((km, kmi))
    kmi = km.keymap_items.new('ovr.render_animation','F12','PRESS',ctrl=True)
    Override_Render_Hotkeys_keymaps.append((km, kmi))

# Hotkey Override --> Removing whole Keymap buggy
def OVR_Property_Override_Render_Hotkeys_Callback(self, context):
    # Globals
    global Override_Render_Hotkeys_keymaps
    if bpy.data.scenes[bpy.context.scene.name].OVR_DriverSettings.Override_Render_Hotkeys == True:
        OVR_Override_Render_Hotkeys_Add_Function()
    else:
        for km, kmi in Override_Render_Hotkeys_keymaps:
            km.keymap_items.remove(kmi)
        Override_Render_Hotkeys_keymaps.clear()
            
############################################################################################################### Render Functions

############################################################################################################### Addon Before Execution Definitions

# Globals Before
addORremove = False
Active_List_Driver = None
DriverList_GlobalList = set()
Driver_Settings_RL = []
Driver_Settings_Default = {}
addon_keymaps = []
Override_Render_Hotkeys_keymaps = []

class OVR_DriverSettings(bpy.types.PropertyGroup):
    DriverAmount_GlobalIndex = bpy.props.IntProperty(default=0,min=0)
    Override_Render_Hotkeys = bpy.props.BoolProperty(name='Override Render Hotkeys',default = False,description ='Override F12 / Ctrl + F12 Hotkeys',update = OVR_Property_Override_Render_Hotkeys_Callback)
    Activate_Abort_Render_Timer_Time = bpy.props.FloatProperty(name='Abort Render Time Frame',default=2,min=0.1,max=60,description ='Time in Seconds the User has to Abort the Render after the Abort Prompt in CMD Window')
    Settings_Default = bpy.props.StringProperty()
    Settings_RL = bpy.props.StringProperty()

def OVR_Default_RenderLayer_Settings_Function(RL):
    RL = bpy.data.scenes[0].render.layers['OVR_Default_RenderLayer']
    RL.use = False
    RL.samples = 1
    RL.use_sky = False
    RL.use_solid = False
    RL.use_strand = False
    for x in range(0,20):
        RL.layers_exclude[x] = True
        RL.layers[9] = True
        RL.layers[x] = False

# Non Animatable Properties Settings/Definitions -> System is based on the fact that all id types have different attribute names (cleaner: check only in necessary id block)
NAP_AcceptDict = {
    "render.use_border":"RENDER_PT_dimensions,Use_Border",
    "render.use_motion_blur":"CyclesRender_PT_motion_blur,Motion_Blur",
    "render.motion_blur_shutter":"CyclesRender_PT_motion_blur,Motion_Blur_Shutter",
    "render.border_min_x":"RENDER_PT_dimensions,Border_Min_X",
    "render.border_max_x":"RENDER_PT_dimensions,Border_Max_X",
    "render.border_min_y":"RENDER_PT_dimensions,Border_Min_Y",
    "render.border_max_y":"RENDER_PT_dimensions,Border_Max_Y",
    "render.tile_x":"CyclesRender_PT_performance,Tile_X",
    "render.tile_y":"CyclesRender_PT_performance,Tile_Y",
    "count":"PARTICLE_PT_emission,Emission"
} 

NAP_Warning_Panel_Functions = []
NAP_Frame_Handler_Functions = []
NAP_Frame_Handler_Skip = False

def NAP_dict_value_search(dict, string):
    for k in dict:
        if dict[k][dict[k].rfind(",")+1:] == string:
            return k
    return None
    
class OVR_Fake_Operator(bpy.types.Operator):
    bl_idname = "ovr.fake_operator"
    bl_label = "Fake Operator"
    bl_description = "OVERRIDE Warning for normally non animatable Properties"
    bl_options = {"REGISTER","UNDO"}

    def execute(self, context):
        return {"FINISHED"}

def OVR_Border_Co_Menu(self, context):
    scene = bpy.context.scene.render
    if scene.use_border == True:      
        layout = self.layout
        row = layout.row()
        col = row.column(align=True)
        col.prop(scene, 'border_min_x', text="Border Min X")
        col.prop(scene, 'border_max_x', text="Border Max X")
        col = row.column(align=True)
        col.prop(scene, 'border_min_y', text="Border Min Y")
        col.prop(scene, 'border_max_y', text="Border Max Y")

def NAP_Frame_Change_Post_Handler_FunctionBuilder(id_block, c_prop_name, prop_name, DriverList_GlobalList_ID): # Create Function Dynamically
    def NAP_Frame_Handler(scene):
        id_block # for some reason variable has to mentioned before it can be used in exec below
        global NAP_Frame_Handler_Skip
        if NAP_Frame_Handler_Skip == False:
            exec("id_block."+prop_name+" = id_block."+c_prop_name)
    global NAP_Frame_Handler_Functions
    NAP_Frame_Handler.__name__ = DriverList_GlobalList_ID
    NAP_Frame_Handler_Functions.append(NAP_Frame_Handler)
    return NAP_Frame_Handler    

def NAP_Warning_Panel_FunctionBuilder(Warning_Text,id_block_type): # Create Function Dynamically
    global NAP_Warning_Panel_Functions
    Panel_Function_Exist = False
    for x in NAP_Warning_Panel_Functions:
        if x.__name__ == Warning_Text:
            Panel_Function_Exist = True
    if Panel_Function_Exist == False:
        if id_block_type == "particles":
            id_block_type = "particle_settingss"
        def NAP_Warning_Panel(self, context):
            id_block_active = eval("bpy.context."+id_block_type[:-1])
            compare_value = id_block_active.path_resolve(Warning_Text) 
            if compare_value != -12345610:
                layout = self.layout
                row = layout.row()
                row.alert=True
                NAP_Warning_Text = "OVR Driver for '"+Warning_Text+"'" 
                row.operator("ovr.fake_operator", text=NAP_Warning_Text)
        NAP_Warning_Panel.__name__ = Warning_Text
        NAP_Warning_Panel_Functions.append(NAP_Warning_Panel)
        return NAP_Warning_Panel
    else:
        return None

def NAP_Property_Delete(self):
    global NAP_AcceptDict
    if self.NAP_driver_original_rna_path in NAP_AcceptDict:
        # Reset to Default Value if Property not found
        found = False
        for x in self.driver_base_path.animation_data.drivers:
            if x.data_path == self.driver_rna_path:
                found = True
                break
        if found == False:   
            exec("self.driver_base_path."+self.driver_rna_path+"= -12345610")

        # Check ID_Blocks for Property -> All Default then Delete
        NAP_OVERRIDE_Count = 0
        for x in bpy.data.path_resolve(self.driver_target_id_type.lower()+"s"):
            if NAP_OVERRIDE_Count < 1:
                x.animation_data_create()
                if x.animation_data.drivers.values() != []:
                    for y in x.animation_data.drivers.values():
                        if y.data_path == self.driver_rna_path:
                            NAP_OVERRIDE_Count += 1
            else:
                break
        if NAP_OVERRIDE_Count < 1:
            if self.driver_target_id_type == 'PARTICLE':
                id_type = 'ParticleSettings'
            else:
                id_type = self.driver_target_id_type.lower().title()
            temp_type = "bpy.types."+id_type
            exec("del "+temp_type+"."+self.driver_rna_path) # Commdand only makes Property disconnect from API Access
                
############################################################################################################### Addon Before Execution Definitions

############################################################################################################### Register Functions
  
def register():
  
    # Panels, Ops & Vars
    bpy.utils.register_class(OVR_RenderLayer_OVERRIDE_Render_Menu)
    bpy.utils.register_class(OVR_Render_Image)
    bpy.utils.register_class(OVR_Render_Animation)
    bpy.utils.register_class(OVR_Generate_Compositor_Setup)
    bpy.utils.register_class(OVR_RenderLayer_OVERRIDE)
    bpy.utils.register_class(OVR_AddRenderLayer)
    bpy.utils.register_class(OVR_RemoveRenderLayer)
    bpy.utils.register_class(OVR_DriverList_Template)
    bpy.utils.register_class(OVR_DriverList)
    bpy.utils.register_class(OVR_AddDriver)
    bpy.utils.register_class(OVR_RemoveDriver)
    bpy.utils.register_class(OVR_InsertKeyframe_ListButton)
    bpy.utils.register_class(OVR_RemoveKeyframe_ListButton)
    bpy.utils.register_class(OVR_SetToDefault_ListButton)
    bpy.utils.register_class(OVR_RemoveDriver_ListButton)
    bpy.utils.register_class(OVR_DriverSettings)
    bpy.utils.register_class(OVR_DriverListSettings)
    bpy.utils.register_class(OVR_Fake_Operator)
    bpy.types.RENDER_PT_dimensions.append(OVR_Border_Co_Menu)
    bpy.types.Scene.OVR_DriverListCollection = bpy.props.CollectionProperty(type=OVR_DriverListSettings)
    bpy.types.Scene.OVR_DriverListIndex = bpy.props.IntProperty(default=0,min=0)
    bpy.types.Scene.OVR_ActiveRenderLayerIndex = IntProperty(min=0,update=OVR_Property_ActiveRenderLayer_Callback)
    bpy.types.Scene.OVR_DriverSettings = bpy.props.PointerProperty(type=OVR_DriverSettings)
    
    # App Handlers
    bpy.app.handlers.load_post.append(OVR_Addon_ExecutionFunction)
    bpy.app.handlers.save_pre.append(OVR_FileSaveFunction)
    bpy.app.handlers.scene_update_post.append(OVR_Addon_First_ExecutionFunction) # Post Load Hack    
        
    # KeyMap
    km = bpy.context.window_manager.keyconfigs.addon.keymaps.new(name='Property Editor', space_type='PROPERTIES')
    kmi = km.keymap_items.new('ovr.adddriver','O','PRESS')
    kmi = km.keymap_items.new('ovr.removedriver','O','PRESS',alt=True)
    addon_keymaps.append(km)  
    km = bpy.context.window_manager.keyconfigs.addon.keymaps.new(name='Node Editor', space_type='NODE_EDITOR')
    kmi = km.keymap_items.new('ovr.adddriver','O','PRESS')
    kmi = km.keymap_items.new('ovr.removedriver','O','PRESS',alt=True)
    addon_keymaps.append(km) 
    km = bpy.context.window_manager.keyconfigs.addon.keymaps.new(name='Outliner', space_type='OUTLINER')
    kmi = km.keymap_items.new('ovr.adddriver','O','PRESS')
    kmi = km.keymap_items.new('ovr.removedriver','O','PRESS',alt=True)
    addon_keymaps.append(km) 
    
def unregister():
    
    # Safe Drivers for Add-on Reactivation
    OVR_FileSaveFunction("scene")

    # Remove NAP Panels & NAP Frame Handlers
    global NAP_AcceptDict
    global NAP_Warning_Panel_Functions
    for x in NAP_Warning_Panel_Functions:
        rna_path = NAP_dict_value_search(NAP_AcceptDict, x.__name__)
        temp_panel_name = NAP_AcceptDict[rna_path][:NAP_AcceptDict[rna_path].rfind(",")]
        exec("bpy.types."+temp_panel_name+".remove(x)")

    global NAP_Frame_Handler_Functions
    for x in NAP_Frame_Handler_Functions:
        bpy.app.handlers.frame_change_post.remove(x)
            
    # Panels, Ops & Vars
    bpy.utils.unregister_class(OVR_RenderLayer_OVERRIDE_Render_Menu)
    bpy.utils.unregister_class(OVR_Render_Image)
    bpy.utils.unregister_class(OVR_Render_Animation)
    bpy.utils.unregister_class(OVR_Generate_Compositor_Setup)
    bpy.utils.unregister_class(OVR_RenderLayer_OVERRIDE)
    bpy.utils.unregister_class(OVR_AddRenderLayer)
    bpy.utils.unregister_class(OVR_RemoveRenderLayer)
    bpy.utils.unregister_class(OVR_DriverList_Template)
    bpy.utils.unregister_class(OVR_DriverList)
    bpy.utils.unregister_class(OVR_AddDriver)
    bpy.utils.unregister_class(OVR_RemoveDriver)
    bpy.utils.unregister_class(OVR_InsertKeyframe_ListButton)
    bpy.utils.unregister_class(OVR_RemoveKeyframe_ListButton)
    bpy.utils.unregister_class(OVR_SetToDefault_ListButton)
    bpy.utils.unregister_class(OVR_RemoveDriver_ListButton)
    bpy.utils.unregister_class(OVR_DriverSettings)
    bpy.utils.unregister_class(OVR_DriverListSettings)
    bpy.utils.unregister_class(OVR_Fake_Operator)
    bpy.types.RENDER_PT_dimensions.remove(OVR_Border_Co_Menu)

    # App Handlers
    bpy.app.handlers.load_post.remove(OVR_Addon_ExecutionFunction)
    bpy.app.handlers.save_pre.remove(OVR_FileSaveFunction)

    # KeyMap
    for km in addon_keymaps:
        bpy.context.window_manager.keyconfigs.addon.keymaps.remove(km)
    addon_keymaps.clear()

if __name__ == "__main__":
    register()

############################################################################################################### Register Functions